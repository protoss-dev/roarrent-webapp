// @author  Natthakit  Poltirach
// @Create Date  2019-09-03
// @Update Date  2019-10-02
var full_url = document.URL; // Get current url
var url_array = full_url.split('=') // Split the string into an array with / as separator
var id_books = url_array[url_array.length - 1];  // Get the last part of the array (-1)
var dataBooks;
var date = Date.now();
var calendar = new Date(date);
var day = ("0" + calendar.getDate()).slice(-2);
var month = ("0" + (calendar.getMonth() + 1)).slice(-2);
var today = (day)+"-"+(month)+"-"+calendar.getFullYear();
$(document).ready(function(){
    getHomeRent()
    //สิ้นสุด get ข้อมูลการจอง

});
// Function : getHomeRent()
// Manage : Get ค่าการจองตามบ้านที่ได้กดเลือกไว้ก่อนหน้า
function getHomeRent() {
// get ข้อมูลการจอง
   // console.log(id_books);
    dataBooks = $.ajax({
        url: "http://localhost:8080/web/homeBooks?id=" + id_books,
        headers: {
            Accept: "application/json",
        },
        type: "GET",
        async: false,
    }).responseJSON;
   // console.log(dataBooks);
    $('div[id="dateCancel"]').append(" "+today)
    $('div[id="nameHome"]').append(" "+dataBooks.home.homeAddress.nameHome)
    $('span[id="numberHome"]').append(" "+dataBooks.home.homeAddress.numberHome)
    $('span[id="villageNo"]').append(" "+dataBooks.home.homeAddress.villageNo)
    $('span[id="village"]').append(" "+dataBooks.home.homeAddress.village)
    $('span[id="alley"]').append(" "+dataBooks.home.homeAddress.alley)
    $('span[id="road"]').append(" "+dataBooks.home.homeAddress.road)
    $('span[id="district"]').append(" "+dataBooks.home.homeAddress.district)
    $('span[id="prefecture"]').append(" "+dataBooks.home.homeAddress.prefecture)
    $('span[id="county"]').append(" "+dataBooks.home.homeAddress.county)
    $('span[id="post"]').append(" "+dataBooks.home.homeAddress.post)
    $('span[id="reservCost"]').append(" "+dataBooks.contract.bail)
}
// Function : updateHomeRent()
// Manage : Post ข้อมูลยกเลิกการจองไปยัง Backend และทำการ Set ข้อมูลให้เป็นการยกเลิกการเช่า
function updateHomeRent() {
    var date = Date.now();
    var calendar = new Date(date);
    var day = ("0" + calendar.getDate()).slice(-2);
    var month = ("0" + (calendar.getMonth() + 1)).slice(-2);
    var today = calendar.getFullYear()+"-"+(month)+"-"+(day) ;
    var Cancel = {
        "id_books":id_books,
        "today":today,
        "id_user":2
    }
    // console.log(Cancel);
    $.ajax({
        url: "http://localhost:8080/web/updateHomeRent",
        headers:{
            Accept:"application/json",
        },
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        type:"POST",
        async:false,
        data: JSON.stringify(Cancel),
        complete:function (xhr) {
        }
    })
}
// Function : CancelRent()
// Manage : แจ้งเตือนสำหรับเยกเลิกการเช่าบ้านสำเร็จ และไม่สำเร็จ
function CancelRent(){
    Swal.fire({
        title: 'ยกเลิกการเช่าหรือไม่',
        text: "ออกก่อนกำหนดสัญญาคุณจะเสียค่าเงินประกัน",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
    }).then((result) => {
        if (result.value) {
            updateHomeRent();
            Swal.fire({
                position: 'center',
                type: 'success',
                title: 'ยกเลิกการเช่าเสร็จสิ้น',
                showConfirmButton: false,
                timer: 2000
            }).then((result) => {
                if (result.value) {
                    var link = document.createElement("a");
                    link.href = "http://localhost:8080/web/renter_manage";
                    link.click();
                } else{
                    link = document.createElement("a");
                    link.href = "http://localhost:8080/web/renter_manage";
                    link.click();
                }
            });
        }
    });
}