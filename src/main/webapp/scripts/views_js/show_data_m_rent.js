/*
@author  Natthakit  Poltirach
@Create Date  2019-09-18
@Update Date  -
*/
var data;
var dataBooks;
var data1;
var data2;
var cost;
$(document).ready(function() {
    data = $.ajax({
        url: "http://localhost:8080/web/showDetailHome",
        headers: {
            Accept: "application/json",
        },
        type: "GET",
        async: false,
    }).responseJSON;
 //   console.log(data);
    dataBooks = $.ajax({
        url: "http://localhost:8080/web/listBooks",
        headers: {
            Accept: "application/json",
        },
        type: "GET",
        async: false,
    }).responseJSON;
    SearchCheck();
    listRentHome();
   // console.log(dataBooks);
    $("#tb_list").DataTable({
        bDestroy: true,
        processing: true,
        "language": {
            "sProcessing": "กำลังดำเนินการ...",
            "sLengthMenu": "แสดง  _MENU_ แถว",
            "sZeroRecords": "ไม่พบข้อมูล",
            "sInfo": "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
            "sInfoEmpty": "แสดง 0 ถึง 0 จาก 0 แถว",
            "sInfoFiltered": "(กรองข้อมูล _MAX_ ทุกแถว)",
            "sInfoPostFix": "",
            "sSearch": "ค้นหา:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "เริ่มต้น",
                "sPrevious": "ก่อนหน้า",
                "sNext": "ถัดไป",
                "sLast": "สุดท้าย"
            }
        }
    });
});
// Function : listHome()
// Manage : แสดงรายการบ้านที่ต้องการจอง/เช่า
function listHome(i){
    $('div[id="card-list"]')
        .append($("<div>").addClass("row")
            .append($("<div>").addClass("container-fluid")
                .append($("<div>").addClass("row")
                    .append($("<div>").addClass("col-sm-1")).append($("<jsp:text>"))
                    .append($("<div>").addClass("col-sm-10")
                        .append($("<div>").addClass("row")
                            .append($("<a>").attr("href", "reservation?id=" + data[i].id_home)
                                .append($("<div>").addClass("panel panel-default").attr("data-toggle", "tooltip").attr("title", "ดูรายละเอียดเพิ่มเติม")
                                    .append($("<div>").addClass("panel-body card-list")
                                        .append($("<div>").addClass("row")
                                            .append($("<div>").addClass("col-sm-4")
                                                .append($("<img>").addClass("gallery").attr("src","http://localhost:8080/web/resources/images/imagesHome/"+data[i].img_home[0].img_home+".png").css('width', '100%').css('height', '200px'))
                                            )
                                            .append($("<div>").addClass("col-sm-8")
                                                .append($("<div>").text("ชื่อบ้าน : " + data[i].homeAddress.nameHome))
                                                .append($("<div>").text("เลขที่้บ้าน : " + data[i].homeAddress.numberHome + "  หมู่ที่ : " + data[i].homeAddress.villageNo + "  หมู่บ้าน : " + data[i].homeAddress.village + "  ตรอก/ซอย : " + data[i].homeAddress.alley + "  ถนน : " + data[i].homeAddress.road))
                                                .append($("<div>").text("ตำบล : " + data[i].homeAddress.district + "  อำเภอ : " + data[i].homeAddress.prefecture + " จังหวัด : " + data[i].homeAddress.county + "  รหัสไปรษณีย์ : " + data[i].homeAddress.post))
                                                .append($("<div>").text("ขนาดบ้าน : " + data[i].house_size + " ตารางวา  ขนาดห้อง : " + data[i].room_size + " ตารางวา" + "  จำนวนห้องน้ำ : " + data[i].number_toilet + " ห้อง"))
                                                .append($("<div>").text("จำนวนชั้น : " + data[i].number_floors + " ชั้น  จำนวนห้อง : " + data[i].number_room + " ห้อง"))
                                                .append($("<div>").text("ค่าน้ำ/หน่วย : " + data[i].cost_water + " บาท  ค่าน้ำ/หน่วย : " + data[i].cost_fire + " บาท"))
                                                .append($("<div>").text("ค่าเช่า/เดือน : " + data[i].cost_hire + " บาท"))
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            )
        )

}
// Function  : listRentHome()
// Manage : แสดงบ้านที่ถูกเช่าลงตารางประวัติการเช่าบ้าน
function listRentHome() {
    var count = 1;
    var status_home = "";
    var color = "";
    var bgColor = "";
    var text = "";
    var img = "";
    var number = "";
    // loop แสดงบ้านที่ถูกเช่าลงตารางประวัติการเช่าบ้าน
    for (var i = 0; i < dataBooks.length; i++) {
        if(dataBooks[i].status_book == 0){
            status_home = "ยกเลิก";
            color = "#F08080";
            bgColor = "#F5F5F5";
            text = "darkgray";
            img = "opacity:0.5";
            number = "darkgray";
        }else if(dataBooks[i].status_book == 1){
            status_home = "จอง";
            color = "orange";
            bgColor = "#fff";
            text = "black";
            img = "opacity:1";
            number = "black";
        }else if(dataBooks[i].status_book == 2){
            status_home = "เช่า";
            color = "green";
            bgColor = "#fff";
            text = "black";
            img = "opacity:1";
            number = "black";
        }else if(dataBooks[i].status_book == 3){
            status_home = "ย้ายออก";
            color = "darkgray";
            bgColor = "#F5F5F5";
            text = "darkgray";
            img = "opacity:0.5";
            number = "darkgray";
        }
        $('tbody[id="tb_detail"]')
            .append($("<tr>").attr("style", "height:100px").css("background-color",bgColor)
                .append($("<td>").addClass("text-center").html(count++).css("color",number))
                .append($("<td>")
                    .append($("<div>").addClass("gallery")
                        .append($("<img>").addClass("gallery").attr("src","http://localhost:8080/web/resources/images/imagesHome/"+ dataBooks[i].home.img_home[0].img_home+".png").attr("style",img))
                    )
                )
                .append($("<td>").attr("style","padding:15px").html("ชื่อบ้าน : " + dataBooks[i].home.homeAddress.nameHome).css("color",text)
                    .append($("<div>").html("เลขที่้บ้าน : " + dataBooks[i].home.homeAddress.numberHome + "&emsp;หมู่ที่ : " + dataBooks[i].home.homeAddress.villageNo + "&emsp;หมู่บ้าน : " + dataBooks[i].home.homeAddress.village ))
                    .append($("<div>").html("ตรอก/ซอย : " + dataBooks[i].home.homeAddress.alley + "&emsp;ถนน : " + dataBooks[i].home.homeAddress.road + "&emsp;ตำบล : " + dataBooks[i].home.homeAddress.district))
                    .append($("<div>").html("อำเภอ : " + dataBooks[i].home.homeAddress.prefecture + "&emsp;จังหวัด : " + dataBooks[i].home.homeAddress.county))
                    .append($("<div>").html("รหัสไปรษณีย์ : " + dataBooks[i].home.homeAddress.post))
                )
                .append($("<td>").attr("style", "valign:middle").addClass("text-center")
                    .append($("<span>").css("color",color)
                        .append($("<b>")
                            .append($("<h2>").html(status_home))
                        )
                    )
                )
                .append($("<td>").addClass("text-center middle").attr("id","statusBooks"+i)
                )
            )
        if(dataBooks[i].status_book == 0){
            $('#statusBooks'+i)
                .append($("<a>").attr("disabled",true).attr("title", "รายละเอียดบ้านเช่า").attr("data-toggle", "tooltip").attr("style", "height:45px").addClass("btn btn-primary")//.attr("href", "home_detail?id=" + dataBooks[i].home.id_home)
                    .append($("<img>").attr("alt", "Lights").attr("src", "/web/resources/images/search.png"))
                ).append("&ensp;")
                .append($("<a>").attr("disabled",true)
                    .append($("<button>").attr("disabled",true).attr("title", "ยกเลิกการจอง/เช่า").attr("style", "height:45px").attr("data-target", "#cancel tooltip").attr("data-toggle", "modal").addClass("btn btn-danger").attr("type", "button")
                        .append($("<img>").attr("alt", "Lights").attr("src", "/web/resources/images/cancel2.png"))
                    )
                )
        }
        else if(dataBooks[i].status_book == 1){
            $('#statusBooks'+i)
                .append($("<a>").attr("title", "รายละเอียดบ้านเช่า").attr("data-toggle", "tooltip").attr("style", "height:45px").addClass("btn btn-primary").attr("href", "home_detail?id=" + dataBooks[i].home.id_home)
                    .append($("<img>").attr("alt", "Lights").attr("src", "/web/resources/images/search.png"))
                ).append("&ensp;")
                .append($("<a>").attr("href", "ReservCancel?id=" + dataBooks[i].id_book)
                    .append($("<button>").attr("title", "ยกเลิกการจอง/เช่า").attr("style", "height:45px").attr("data-target", "#cancel tooltip").attr("data-toggle", "modal").addClass("btn btn-danger").attr("type", "button")
                        .append($("<img>").attr("alt", "Lights").attr("src", "/web/resources/images/cancel2.png"))
                    )
                )
        }
        else if(dataBooks[i].status_book == 2){
            $('#statusBooks'+i)
                .append($("<a>").attr("title", "รายละเอียดบ้านเช่า").attr("data-toggle", "tooltip").attr("style", "height:45px").addClass("btn btn-primary").attr("href", "home_detail?id=" + dataBooks[i].home.id_home)
                    .append($("<img>").attr("alt", "Lights").attr("src", "/web/resources/images/search.png"))
                ).append("&ensp;")
                .append($("<a>").attr("href", "RentCancel?id=" + dataBooks[i].id_book)
                    .append($("<button>").attr("title", "ยกเลิกการจอง/เช่า").attr("style", "height:45px").attr("data-target", "#cancel tooltip").attr("data-toggle", "modal").addClass("btn btn-danger").attr("type", "button")
                        .append($("<img>").attr("alt", "Lights").attr("src", "/web/resources/images/cancel2.png"))
                    )
                )
        }
        else if(dataBooks[i].status_book == 3){
            $('#statusBooks'+i)
                .append($("<a>").attr("disabled",true).attr("title", "รายละเอียดบ้านเช่า").attr("data-toggle", "tooltip").attr("style", "height:45px").addClass("btn btn-primary")//.attr("href", "home_detail?id=" + dataBooks[i].home.id_home)
                    .append($("<img>").attr("alt", "Lights").attr("src", "/web/resources/images/search.png"))
                ).append("&ensp;")
                .append($("<a>")
                    .append($("<button>").attr("disabled",true).attr("title", "ยกเลิกการจอง/เช่า").attr("style", "height:45px").attr("data-target", "#cancel tooltip").attr("data-toggle", "modal").addClass("btn btn-danger").attr("type", "button")
                        .append($("<img>").attr("alt", "Lights").attr("src", "/web/resources/images/cancel2.png"))
                    )
                )
        }
    }
}
// Function  : SearchCheck()
// Manage : ตรวจสอบค่าในส่วนของการค้นหาบ้านเช่า
function SearchCheck(){
    //loop check ค่าของช่อง startCost และ endCost
    $("#card-list").empty();
    if($('input[id="startCost"]').val() != "" && $('input[id="endCost"]').val() != ""){
        if($('input[id="startCost"]').val() > $('input[id="endCost"]').val()){
            Swal.fire({
                type: 'error',
                title: 'ข้อมูลผิดพลาด',
                text: 'ราคาเริ่มต้นต้องน้อยกว่าราคาสิ้นสุด!',
            })
        }
        else if($('input[id="startCost"]').val() <= $('input[id="endCost"]').val()) {
        data1 = $('input[id="startCost"]').val();
        data2 = $('input[id="endCost"]').val();
        cost = 1
        }
        // console.log(cost);
    }
    else if($('input[id="startCost"]').val() == "" && $('input[id="endCost"]').val() != ""){
        data1 = 0;
        data2 = $('input[id="endCost"]').val();
        cost = 1
            // console.log(cost);
    }
    else if($('input[id="startCost"]').val() != "" && $('input[id="endCost"]').val() == ""){
        data1 = $('input[id="startCost"]').val();
        data2 = 1000000;
        cost = 1
            // console.log(cost);
    }
    else if($('input[id="startCost"]').val() == "" && $('input[id="endCost"]').val() == ""){
        data1 = 0;
        data2 = 1000000;
        cost = 0;
            // console.log(cost);
    }    //สิ้นสุด loop check ค่าของช่อง startCost และ endCost
    //loop check ค่าของช่องค้นหาทั้งหมด
    if($('input[id="place"]').val() == "" && $('input[id="room"]').val() == "" && cost == 0){
        // console.log("ว่าง");
        for (var i = 0; i < data.length; i++) {
            if(data[i].status_home == "1") {
                listHome(i);
            }
        }
    }
    else if($('input[id="place"]').val() != "" && $('input[id="room"]').val() == "" && cost == 0){
        // console.log($('input[id="place"]').val()+ " " + $('input[id="room"]').val());
        for (var i = 0; i < data.length; i++) {
            if($('input[id="place"]').val() == data[i].homeAddress.county && data[i].status_home == "1" && data[i].cost_hire >= data1 && data[i].cost_hire <= data2){
                listHome(i);
            }
        }
    }
    else if($('input[id="place"]').val() != "" && $('input[id="room"]').val() != "" && cost == 0){
        // console.log($('input[id="place"]').val()+ " " + $('input[id="room"]').val());
        for (var i = 0; i < data.length; i++) {
            if($('input[id="place"]').val() == data[i].homeAddress.county && $('input[id="room"]').val() == data[i].number_room && data[i].status_home == "1" && data[i].cost_hire >= data1 && data[i].cost_hire <= data2){
                listHome(i);
            }
        }
    }
    else if($('input[id="place"]').val() != "" && $('input[id="room"]').val() != "" && cost == 1){
        // console.log($('input[id="place"]').val()+ " " + $('input[id="room"]').val()+ " " + data1 + " " + data2);
        for (var i = 0; i < data.length; i++) {
            if($('input[id="place"]').val() == data[i].homeAddress.county && $('input[id="room"]').val() == data[i].number_room && data[i].status_home == "1" && data[i].cost_hire >= data1 && data[i].cost_hire <= data2){
                listHome(i);
            }
        }
    }
    else if($('input[id="place"]').val() == "" && $('input[id="room"]').val() != "" && cost == 0){
        // console.log($('input[id="place"]').val()+ " " + $('input[id="room"]').val());
        for (var i = 0; i < data.length; i++) {
            if($('input[id="room"]').val() == data[i].number_room && data[i].status_home == "1" && data[i].cost_hire >= data1 && data[i].cost_hire <= data2){
                listHome(i);
            }
        }
    }
    else if($('input[id="place"]').val() == "" && $('input[id="room"]').val() != "" && cost == 1){
        // console.log($('input[id="place"]').val()+ " " + $('input[id="room"]').val()+ " " + data1 + " " + data2);
        for (var i = 0; i < data.length; i++) {
            if($('input[id="room"]').val() == data[i].number_room && data[i].status_home == "1" && data[i].cost_hire >= data1 && data[i].cost_hire <= data2){
                listHome(i);
            }
        }
    }
    else if($('input[id="place"]').val() == "" && $('input[id="room"]').val() == "" && cost == 1){
        // console.log($('input[id="place"]').val()+ " " + $('input[id="room"]').val()+ " " + data1 + " " + data2);
        for (var i = 0; i < data.length; i++) {
            if(data[i].status_home == "1" && data[i].cost_hire >= data1 && data[i].cost_hire <= data2){
                listHome(i);
            }
        }
    }
    else if($('input[id="place"]').val() != "" && $('input[id="room"]').val() == "" && cost == 1){
        // console.log($('input[id="place"]').val()+ " " + $('input[id="room"]').val()+ " " + data1 + " " + data2);
        for (var i = 0; i < data.length; i++) {
            if($('input[id="place"]').val() == data[i].homeAddress.county && data[i].status_home == "1" && data[i].cost_hire >= data1 && data[i].cost_hire <= data2){
                listHome(i);
            }
        }
    }
    //สิ้นสุด loop check ค่าของช่องค้นหาทั้งหมด
}
