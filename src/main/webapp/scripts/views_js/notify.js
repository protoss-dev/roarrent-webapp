/*
@author  Sarawut Promsoon
@Create Date
@Update Date
* file.js : notify
* Manage : javaScript form v_notify
*/

var data;
$(document).ready(function() {
    getDataList();
    SearchAll();
    // ---menu---
    $("#all")
        .css("background-color", "#3498DB")
        .css("color", "#FBFCFC");

    $("#all").click(function() {
        $("#all")
            .css("background-color", "#3498DB")
            .css("color", "#FBFCFC");
        $("#book")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
        $("#issue")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
        $("#out")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
    });
    $("#book").click(function() {
        $("#all")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
        $("#book")
            .css("background-color", "#3498DB")
            .css("color", "#FBFCFC");
        $("#issue")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
        $("#out")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
    });
    $("#issue").click(function() {
        $("#all")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
        $("#book")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
        $("#issue")
            .css("background-color", "#3498DB")
            .css("color", "#FBFCFC");
        $("#out")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
    });
    $("#out").click(function() {
        $("#all")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
        $("#book")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
        $("#issue")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
        $("#out")
            .css("background-color", "#3498DB")
            .css("color", "#FBFCFC");
    });
    $("#status").click(function(){
        if($("#status").val()=="2"){
            $("#status").css("background-color","#5cb85c").css("color","#fff");
        }else if($("#status").val()=="1"){
            $("#status").css("background-color","#e5772c").css("color","#fff");
        }else if($("#status").val()=="0"){
            $("#status").css("background-color","#d43f3a").css("color","#fff");
        }
    })
});
function shadow(x) {
    x.classList.add("shadow");
}

function noShadow(x) {
    x.classList.remove("shadow");
}
function getDataList() {
    // console.log(new Date(1549312452 * 1000).toISOString().slice(0, 19).replace('T', ' '));
    data = $.ajax({
        url: "http://localhost:8080/web/showNotify",
        headers:{
            Accept:"application/json",
        },
        type:"GET",
        async:false,
    }).responseJSON;
    console.log(data);
}
function formatDate(numberTime){
    var timestamp = new Date(numberTime);
    var d = timestamp.getDate().toString();
    var dd = (d.length === 2) ? d : "0"+d;
    var m = (timestamp.getMonth()+1).toString();
    var mm = (m.length === 2) ? m : "0"+m;
    return (dd+"/"+mm+ "/" + (timestamp.getFullYear()).toString());
}
// function showListData(i) {
//
//     var numberTime = parseInt(data[i].date_submit);
//     var date = formatDate(numberTime);
//
//     if(data[i].topic == "1") {
//         nameTopic = "จอง"
//     }else if(data[i].topic == "2"){
//         nameTopic = "ปัญหา"
//     }else if(data[i].topic == "3"){
//         nameTopic = "ย้ายออก"
//     }
//
//     $("#panel_start").append($("<div>")
//         .addClass("panel panel-default").attr("id","panel"+data[i].id_inform).attr("onmouseover","shadow(this)").attr("onmouseout","noShadow(this)")
//         .append($("<div>").addClass("panel-body")
//             .append($("<div>").addClass("row")
//                 .append($("<div>").addClass("col-sm-12")
//                     .append($("<div>").addClass("col-sm-3")
//                         .append($("<div>").addClass("gallery")
//                             .append($("<img>").attr("src","https://www.hba-th.org/images/kachaporn.png"))
//                         )
//                     )
//                     .append($("<div>").addClass("col-sm-7")
//                         .append($("<div>").addClass("text-justify")
//                             .append($("<div>").append($("<U>").append($("<strong>").append($("<font>").attr("size","4").text("หัวข้อเรื่อง :"+nameTopic)))))
//                             .append($("<div>").text("วันที่แจ้ง :"+date))
//                             .append($("<div>").text("รายละเอียด :"+data[i].desciption))
//                         )
//                     )
//                     .append($("<div>").addClass("col-sm-2 text-center")
//                         .append($("<div>").addClass("row")
//                             .append($("<select>").addClass("form-control").attr("id","status"+data[i].id_inform).attr("onChange","updateStatus("+data[i].id_inform+")").css("height","50px")
//                                 .append($("<option>").attr("disabled","ture").css("background-color","rgb(255, 255, 255)").css("color","black").text("--เลือก--"))
//                                 .append($("<optgroup>").attr("label","ดำเนินการ").css("background-color","rgb(255, 255, 255)").css("color","black")
//                                     .append($("<option>").attr("value","2").css("background-color","rgb(255, 255, 255)").css("color","black").text("เสร็จสิ้น"))
//                                     .append($("<option>").attr("value","1").css("background-color","rgb(255, 255, 255)").css("color","black").text("รอดำเนินการ"))
//                                     .append($("<option>").attr("value","0").css("background-color","rgb(255, 255, 255)").css("color","black").text("ยกเลิก"))
//                                 )
//                                 .append($("<optgroup>").attr("label","สัญญา").css("background-color","rgb(255, 255, 255)").css("color","black")
//                                     .append($("<option>").attr("value","opel").css("background-color","rgb(255, 255, 255)").css("color","black").text("พิมพ์สัญญา"))
//                                 )
//                             )
//                         )
//                     )
//                 )
//             )
//         )
//     )
//     if(data[i].status_inform == 2){
//         $("#panel"+data[i].id_inform).removeClass("panel-default").addClass("panel-success");
//         $("#status"+data[i].id_inform).css("background-color",'rgba(2,255,4,0.8)');
//     }else if(data[i].status_inform == 1){
//         $("#panel"+data[i].id_inform).removeClass("panel-default").addClass("panel-warning");
//         $("#status"+data[i].id_inform).css("background-color",'rgba(255,200,0,0.81)');
//     }else if(data[i].status_inform == 0){
//         $("#panel"+data[i].id_inform).removeClass("panel-default").addClass("panel-danger");
//         $("#status"+data[i].id_inform).css("background-color",'rgba(255,54,28,0.8)');
//     }
// }
function SearchAll(){
    // === list RQ ===
    $("#panel_start").empty()
    var nameTopic;
    for(var i = 0 ; i < data.length ; i++){
        if(data[i].home.user.id_user == 1){
            showListData(i);
        }else{
        }
        seleted(i);
    }
}
function SearchBook() {
// === list RQ ===
    $("#panel_start").empty();
    var nameTopic;
    for(var i = 0 ; i < data.length ; i++){
        if(data[i].home.user.id_user == 1 && data[i].topic == "1"){
            showListData(i);
        }else{
        }
        seleted(i);
    }
}
function SearchIssue() {
// === list RQ ===
    $("#panel_start").empty();
    var nameTopic;
    for(var i = 0 ; i < data.length ; i++){
        if(data[i].home.user.id_user == 1 && data[i].topic == "2"){
            showListData(i);
        }else{
        }
        seleted(i);
    }
}
function SearchMove() {
// === list RQ ===
    $("#panel_start").empty();
    var nameTopic;
    for(var i = 0 ; i < data.length ; i++){
        if(data[i].home.user.id_user == 1 && data[i].topic == "3"){
            showListData(i);
        }else{
        }
        seleted(i);
    }
}

function updateStatus(id) {
    console.log(id);
    console.log("status val :"+$("#status").val());
    var edit = {
        "status":$("#status").val(),
        "id_inform":id
    }
    $.ajax({
        url: "http://localhost:8080/web/updateStatusInform",
        headers:{
            Accept:"application/json",
        },
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        type:"POST",
        async:false,
        data: JSON.stringify(edit),
        complete:function (xhr) {
        }
    })
    getDataList();
    SearchAll();
}
function seleted(i) {
    $("#status"+data[i].id_inform+" option[value="+data[i].status_inform+"]").attr("selected", "true");
}

function showListData(i){
    var numberTime = parseInt(data[i].date_submit);
    var date = formatDate(numberTime);
    var statusName = "";
    var colorText = "";
    var numberTopic = 0;

    if(data[i].topic == "0") {
        nameTopic = "ยกเลิกจอง";
        numberTopic = 0;
    }else if(data[i].topic == "1") {
        nameTopic = "จอง";
        numberTopic = 1;
    }else if(data[i].topic == "2"){
        nameTopic = "ปัญหา";
        numberTopic = 2;
    }else if(data[i].topic == "3"){
        nameTopic = "ย้ายออก";
        numberTopic = 3;
    }

    if(data[i].status_inform == 2){
        statusName = "เสร็จสิ้น"
        colorText = "rgba(2,255,4,0.8)"
    }else if(data[i].status_inform == 1){
        statusName = "รอดำเนินการ"
        colorText = "rgba(255,200,0,0.81)"
    }else if(data[i].status_inform == 0){
        statusName = "ยกเลิก"
        colorText = "rgba(255,54,28,0.8)"
    }
    if(data[i].topic == "1"){
        $("#panel_start").append($("<div>")
            .addClass("panel panel-default").attr("id","panel"+data[i].id_inform).attr("onmouseover","shadow(this)").attr("onmouseout","noShadow(this)")
            .append($("<div>").addClass("panel-body")
                .append($("<div>").addClass("row")
                    .append($("<div>").addClass("col-sm-12")
                        .append($("<div>").addClass("col-sm-3")
                            .append($("<div>").addClass("gallery")
                                .append($("<img>").attr("src","http://localhost:8080/web/resources/images/imagesHome/"+data[i].home.img_home[0].img_home+".png"))
                            )
                        )
                        .append($("<div>").addClass("col-sm-7")
                            .append($("<div>").addClass("text-justify")
                                .append($("<div>").append($("<U>").append($("<strong>").append($("<font>").attr("size","4").text("หัวข้อเรื่อง :"+nameTopic)))))
                                .append($("<div>").text("วันที่แจ้ง :"+date))
                                .append($("<div>").text("รายละเอียด :"+data[i].desciption))
                                .append($("<div>").text("เบอร์ติดต่อ :"+data[i].user.phone))
                            )
                        )
                        .append($("<div>").addClass("col-sm-2 ")
                            .append($("<div>").addClass("search-right")
                                .append($("<div>").addClass("dropdown").attr("onChange","updateStatus("+data[i].id_inform+")")
                                    .append($("<button>").addClass("btn btn-default dropdown-toggle").attr("type","button").attr("data-toggle","dropdown")
                                        .append($("<span>").addClass("caret").append($("<jsp:text/>")))
                                    )
                                    .append($("<ul>").addClass("dropdown-menu")
                                        .append($("<li>").addClass("dropdown-header").text("รายการ"))
                                        .append($("<li>")
                                            .append($("<a>").attr("data-toggle","modal").attr("data-target","#myModal").attr("onclick","setDataModal("+i+","+numberTopic+","+data[i].date_submit+")")
                                                .append($("<i>").addClass("glyphicon glyphicon-search").append($("<jsp:text/>")))
                                                .append(" ดูรายละเอียด")
                                            )
                                        )
                                        .append($("<li>")
                                            .append($("<a>")
                                                .append($("<i>").addClass("glyphicon glyphicon-print").append($("<jsp:text/>")))
                                                .append(" พิมพ์สัญญา")
                                            )

                                        )
                                    )
                                )
                            )
                            .append($("<div>").addClass("row text-center").attr("id","statusName").css("color",colorText)
                                .append($("<b>").append($("<h3>").text(statusName)))
                            )
                        )
                    )
                )
            )
        )

    }else{
        $("#panel_start").append($("<div>")
            .addClass("panel panel-default").attr("id","panel"+data[i].id_inform).attr("onmouseover","shadow(this)").attr("onmouseout","noShadow(this)")
            .append($("<div>").addClass("panel-body")
                .append($("<div>").addClass("row")
                    .append($("<div>").addClass("col-sm-12")
                        .append($("<div>").addClass("col-sm-3")
                            .append($("<div>").addClass("gallery")
                                .append($("<img>").attr("src","http://localhost:8080/web/resources/images/imagesHome/"+data[i].home.img_home[0].img_home+".png"))
                            )
                        )
                        .append($("<div>").addClass("col-sm-7")
                            .append($("<div>").addClass("text-justify")
                                .append($("<div>").append($("<U>").append($("<strong>").append($("<font>").attr("size","4").text("หัวข้อเรื่อง :"+nameTopic)))))
                                .append($("<div>").text("วันที่แจ้ง :"+date))
                                .append($("<div>").text("รายละเอียด :"+data[i].desciption))
                                .append($("<div>").text("เบอร์ติดต่อ :"+data[i].user.phone))
                            )
                        )
                        .append($("<div>").addClass("col-sm-2 ")
                            .append($("<div>").addClass("search-right")
                                .append($("<div>").addClass("dropdown").attr("onChange","updateStatus("+data[i].id_inform+")")
                                    .append($("<button>").addClass("btn btn-default dropdown-toggle").attr("type","button").attr("data-toggle","dropdown")
                                        .append($("<span>").addClass("caret").append($("<jsp:text/>")))
                                    )
                                    .append($("<ul>").addClass("dropdown-menu")
                                        .append($("<li>").addClass("dropdown-header").text("รายการ"))
                                        .append($("<li>")
                                            .append($("<a>").attr("data-toggle","modal").attr("data-target","#myModal").attr("onclick","setDataModal("+i+","+numberTopic+","+data[i].date_submit+")")
                                                .append($("<i>").addClass("glyphicon glyphicon-search").append($("<jsp:text/>")))
                                                .append(" ดูรายละเอียด")
                                            )
                                        )
                                    )
                                )
                            )
                            .append($("<div>").addClass("row text-center").attr("id","statusName").css("color",colorText)
                                .append($("<b>").append($("<h3>").text(statusName)))
                            )
                        )
                    )
                )
            )
        )
    }


    // if(data[i].status_inform == 2){
    //     $("#statusName").css("color",'rgba(2,255,4,0.8)');
    // }else if(data[i].status_inform == 1){
    //     $("#statusName").css("color",'rgba(255,200,0,0.81)');
    // }else if(data[i].status_inform == 0){
    //     $("#statusName").css("color",'rgba(255,54,28,0.8)');
    // }
}
function setDataModal(id,Topic,date) {
    console.log("idinform : "+id);
    var numberTime = parseInt(date);
    var dateModal = formatDate(numberTime);
    var topic = "";
    if(Topic == 0) {
        topic = "ยกเลิกจอง"
    }else if(Topic == 1) {
        topic = "จอง"
    }else if(Topic == 2){
        topic = "ปัญหา"
    }else if(Topic == 3){
        topic = "ย้ายออก"
    }
    console.log(data)
    console.log(data[id].desciption +" : "+data[id].user.fullName);

    $("#dateModal").empty()
    $("#titleModal").empty()
    $("#detailModal").empty()
    $("#nameUser").empty()
    $("#dateModal").text(dateModal);
    $("#titleModal").text(topic);
    $("#detailModal").text(data[id].desciption);
    $("#nameUser").text(data[id].user.fullName);
    $("#phone").text(data[id].user.phone)

    if(data[id].status_inform == 2){
        $("#status").val(2).css("background-color","#5cb85c").css("color","#fff");
    }else if(data[id].status_inform == 1){
        $("#status").val(1).css("background-color","#e5772c").css("color","#fff");
    }else if(data[id].status_inform == 0){
        $("#status").val(0).css("background-color","#d43f3a").css("color","#fff");
    }
    $("#status").attr("onChange","updateStatus("+data[id].id_inform+")")


}