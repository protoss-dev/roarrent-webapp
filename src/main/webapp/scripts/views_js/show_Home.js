/*
@author  Sarawut Promsoon
@Create Date 2019-09-11
@Update Date
* file.js : show_Home
* Manage : javaScript form v_manage_own
*/

$(document).ready(function() {
    showHome();
    $("#tb_list").DataTable({
        bDestroy: true,
        processing: true,
        "language": {
            "sProcessing": "กำลังดำเนินการ...",
            "sLengthMenu": "แสดง  _MENU_ แถว",
            "sZeroRecords": "ไม่พบข้อมูล",
            "sInfo": "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
            "sInfoEmpty": "แสดง 0 ถึง 0 จาก 0 แถว",
            "sInfoFiltered": "(กรองข้อมูล _MAX_ ทุกแถว)",
            "sInfoPostFix": "",
            "sSearch": "ค้นหา:",
            "sUrl": "",
            "oPaginate": {
                "sFirst": "เริ่มต้น",
                "sPrevious": "ก่อนหน้า",
                "sNext": "ถัดไป",
                "sLast": "สุดท้าย"
            }
        }
    });
    var data = $.ajax({
        url: "http://localhost:8080/web/getDataUser",
        headers:{
            Accept:"application/json",
        },
        type:"GET",
        async:false,
    }).responseJSON;
    //console.log(data);

});


function showHome() {
    var status = "";
    var nameHome = "";
    var data = $.ajax({
        url: "http://localhost:8080/web/showHome",
        headers:{
            Accept:"application/json",
        },
        type:"GET",
        async:false,
    }).responseJSON;
    //console.log(data);
    var indexI = 0;
    for (var i = 0 ;i<data.length;i++){
        if(data[i].user.id_user == 1){
            var sta = data[i].status_home;
            if(sta != 0){
                if(sta == 1){
                    status = "ว่าง";
                }else if(sta == 3){
                    status = "ถูกเช่า";
                }else if(sta == 2){
                    status = "จอง";
                }
                $("#tb_body")
                    .append($("<tr>").css("height","100px")
                        .append($("<td>").addClass("text-center").text(indexI+1))
                        .append($("<td>").append($("<div>").addClass("gallery").append($("<img>").attr("src","http://localhost:8080/web/resources/images/imagesHome/"+data[i].img_home[0].img_home+".png"))))
                        .append($("<td>")
                            .append($("<div>").text("ชื่อบ้าน: "+data[i].homeAddress["nameHome"]))
                            .append($("<div>").attr("id","d"+i).text("ขนาดบ้าน: "+data[i].house_size+" ตารางวา ขนาดห้อง: "+data[i].room_size+" ตารางวา"))
                            .append($("<div>").text("จำนวนชั้น: "+data[i].number_floors+" ชั้น จำนวนห้อง: "+data[i].number_room+" ห้อง"))
                            .append($("<div>").text("จำนวนห้องน้ำ: "+data[i].number_toilet+" ห้อง"))
                            .append($("<div>").text("ค่าน้ำ/หน่วย: "+data[i].cost_water+" บาท ค่าไฟ/หน่วย: "+data[i].cost_fire+" บาท"))
                            .append($("<div>").text("ค่าเช่า/เดือน: "+data[i].cost_hire+" บาท")))

                        .append($("<td>").addClass("text-center").attr("valign","middle").append($("<b>").append($("<h2>").append($("<font>").attr("id","stt"+i).text(status)))))
                        .append($("<td>").addClass("text-center middle").attr("id","manage"+i)
                        )
                    )
                if(sta == 1){
                    $("#stt"+i).css("color","#00FF00");
                    $("#manage"+i).append($("<a>").attr("href","edit_home?id="+data[i].id_home).addClass("btn btn-warning btn-md").attr("data-toggle","tooltip").attr("title","แก้ไขข้อมูล").append($("<i>").addClass("glyphicon glyphicon-pencil").append($("<jsp:text>"))))
                        .append("&#160;")
                        .append($("<a>").attr("onclick","checkDeleteHome("+data[i].id_home+")").addClass("btn btn-danger btn-md").attr("data-toggle","tooltip").attr("title","ลบข้อมูล").append($("<i>").addClass("glyphicon glyphicon-remove").append($("<jsp:text>"))))

                }else{
                    if(sta == 3){
                        $("#stt"+i).css("color","#FF0000")
                    }else if(sta == 2){
                        $("#stt"+i).css("color","#ffb209")
                    }
                    $("#manage"+i).append($("<a>").attr("href","contract?id="+data[i].id_home).addClass("btn btn-default btn-md").attr("data-toggle","tooltip").attr("title","พิมพ์สัญญา").append($("<i>").addClass("glyphicon glyphicon-print").append($("<jsp:text>"))))
                        .append("&#160;")
                        .append($("<a>").attr("href","edit_home?id="+data[i].id_home).addClass("btn btn-warning btn-md").attr("data-toggle","tooltip").attr("title","แก้ไขข้อมูล").append($("<i>").addClass("glyphicon glyphicon-pencil").append($("<jsp:text>"))))
                        .append("&#160;")
                        .append($("<a>").addClass("btn btn-danger btn-md").attr("data-toggle","tooltip").attr("title","ลบข้อมูล").attr("disabled","ture").append($("<i>").addClass("glyphicon glyphicon-remove").append($("<jsp:text>"))))
                }
                indexI++;
            }//sta != 3

        }else{

        }

    }

}
function deleteHome(id) {
    var data = {
        "id":id
    }
    $.ajax({
        url: "http://localhost:8080/web/deleteHome",
        headers:{
            Accept:"application/json",
        },
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        type:"POST",
        async:false,
        data: JSON.stringify(data),
        complete:function (xhr) {
        }
    })
    $("#tb_body").empty();
    showHome();
}
function checkDeleteHome(id) {
    Swal.fire({
        title: 'ต้องการลบ?',
        text: "คุณแน่ใจที่จะลบข้อมูล!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ใช่',
        cancelButtonText: 'ไม่'
    }).then((result) => {
        if (result.value) {
        deleteHome(id);
        Swal.fire({
            position: 'top-end',
            type: 'success',
            title: 'ลบสำเร็จ',
            showConfirmButton: false,
            timer: 1500
        })
    }
})
}
