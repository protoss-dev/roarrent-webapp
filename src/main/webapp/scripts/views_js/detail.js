 // @Author Natthakit Poltirach
 // @Create Date  2019-08-22
 // @Update Date  2019-08-28
 // @Update Date  2019-09-02
var full_url = document.URL; // Get current url
var url_array = full_url.split('=') // Split the string into an array with / as separator
var id_home = url_array[url_array.length - 1];  // Get the last part of the array (-1)
var data ;
var dataInform;
var gallery;
$(document).ready(function(){
    //Get ข้อมูลบ้านเช่า
    //console.log(id_home)
    data = $.ajax({
        url: "http://localhost:8080/web/homeDetail?id=" + id_home,
        headers: {
            Accept: "application/json",
        },
        type: "GET",
        async: false,
    }).responseJSON;
    //console.log(data)
    //เรียกใช้ function แจ้งปัญหา
    $('button[id="btn_report"]').click(function () {
        inform();
    });
    GalleryHome();
    tb_inform();
    reservDetailHome();
    profileRenter();
    btnInform();
    // getInform();

});
// Function : reservDetailHome()
// Manage : แสดงรายละเอียดบ้านเช่า
function reservDetailHome() {
    // console.log(id_user);
    $('div[id="data-home"]')
        .append($("<div>").addClass("row").attr("style", "padding-left:15px")
            .append($("<div>").addClass("container")
                .append($("<div>").addClass("row")
                    .append($("<div>").text("ชื่อบ้าน : " + data.homeAddress.nameHome)
                        .append($("<div>").html("เลขที่บ้าน : " + data.homeAddress.numberHome + "&emsp;หมู่ที่ : " + data.homeAddress.villageNo + "&emsp;หมู่บ้าน : " + data.homeAddress.village + "&emsp;ตรอก/ซอย : " + data.homeAddress.alley + "&emsp;ถนน : " + data.homeAddress.road + "&emsp;ตำบล : " + data.homeAddress.district))
                        .append($("<div>").html("อำเภอ : " + data.homeAddress.prefecture + "&emsp;จังหวัด : " + data.homeAddress.county + "&emsp;รหัสไปรษณีย์ : " + data.homeAddress.post + "&emsp;ขนาดบ้าน : " + data.house_size + " ตารางวา&emsp;ขนาดห้อง : " + data.room_size + " ตารางวา"))
                        .append($("<div>").html("จำนวนห้องน้ำ : " + data.number_toilet + " ห้อง&emsp;จำนวนชั้น : " + data.number_floors + " ชั้น&emsp;จำนวนห้อง : " + data.number_room + " ห้อง"))
                        .append($("<div>").html("ค่าน้ำ/หน่วย : " + data.cost_water + " บาท&emsp;ค่าน้ำ/หน่วย : " + data.cost_fire + " บาท&emsp;ค่าเช่า/เดือน : " + data.cost_hire + " บาท"))
                    )
                )
            )
        )
}
// Function : profileRenter()
// Manage : แสดงรายละเอียดเจ้าของบ้านเช่า
function profileRenter() {
    $('span[id="name"]').html(": " + data.user.fullName)
    $('span[id="facebook"]').html(": " + data.user.line_id)
    $('span[id="gmail"]').html(": " + data.user.email)
    $('span[id="local"]').html(": " + data.user.user_address.number_home + "&emsp;หมู่ : "+ data.user.user_address.village_no + "&emsp;ตำบล : " + data.user.user_address.district)
    $('span[id="local2"]').html("&ensp;อำเภอ : " + data.user.user_address.prefecture+ "&emsp;จังหวัด : " + data.user.user_address.county )
    $('span[id="phone"]').html(": " + data.user.phone)
}
// Function : inform()
// Manage : ดึงข้อมูลการแจ้งจาก Database ลงในตาราง "ประวัติการแจ้งปัญหา"
function inform() {
    var i = 1;
    var d = new Date();
    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    var date = day + "/" + month + "/" + year;
    data_hReport = $('input[id="hReport"]').val();
    data_bReport = $('textarea[id="bReport"]').val();
    $('tbody[id="data"]').append($('<tr>').append($('<td>').addClass("col-md-1 text-center").append(i)).append($('<td>').addClass("col-md-2 text-center").append(data_hReport)).append($('<td >').addClass("col-md-6").append(data_bReport)).append($('<td >').addClass("col-md-1 text-center").append(date)).append($('<td >').addClass("col-md-2 text-center").append("ยังไม่ดำเนินการ")));
    i++;
    $('input[id="hReport"]').val("");
    $('textarea[id="bReport"]').val("");//Script เพิ่มตาราง วัน/เดือน/ปี
}
// Function : btnInform()
// Manage : เพิ่มปุ่มในการแจ้งลงใน panel ประวัติการแจ้งปัญหา
function btnInform(){
    $('div[id="pb_hisInform"]')
        .append($("<a>").attr("href","ProbInform?id=" + data.id_home)
            .append($("<button>").attr("type","button").addClass("btn btn-primary pull-left").attr("data-toggle","modal tooltip").attr("data-target","#inform").attr("style","height:40px").attr("title","แจ้งปัญหา")
                .append($("<span>")
                    .append($("<a>").addClass("glyphicon glyphicon-plus"))
                    .append("&nbsp;").text("แจ้ง").attr("style","font-size:18px")
                )
            )
        )
        .append($("<br>")).append($("<br>")).append($("<br>"))
}
// Function : tb_inform()
// Manage : Get ข้อมูล Inform ลงในตาราง "ประวัติการแจ้งปัญหา"
function tb_inform() {
    //Get ข้อมูล Inform
    var j = 1;
    dataInform = $.ajax({
        url: "http://localhost:8080/web/getInform",
        headers: {
            Accept: "application/json",
        },
        type: "GET",
        async: false,
    }).responseJSON;
    //console.log(dataInform)
    for (var i = 0; i < dataInform.length; i++) {
        var calendar = new Date(dataInform[i].date_submit);
        var day = ("0" + calendar.getDate()).slice(-2);
        var month = ("0" + (calendar.getMonth() + 1)).slice(-2);
        var date = calendar.getFullYear() + "-" + (month) + "-" + (day);
        if (dataInform[i].topic == 2) {
            var topic_status = "แจ้งปัญหา";
        } else if (dataInform[i].topic == 3) {
            var topic_status = "ย้ายออก";
        }
        if (dataInform[i].status_inform == 0) {
            var inform_status = "ถูกยกเลิก";
            var color = "red";
        } else if (dataInform[i].status_inform == 1) {
            var inform_status = "รอดำเนินการ";
            var color = "orange";
        } else if (dataInform[i].status_inform == 2) {
            var inform_status = "เสร็จสิ้น";
            var color = "green";
        }
        if(dataInform[i].home.id_home == id_home && dataInform[i].user.id_user == 2 && dataInform[i].topic == 2){
            $('tbody[id="data"]')
                .append($("<tr>")
                    .append($("<td>").addClass("col-md-1 text-center").html(j))
                    .append($("<td>").addClass("col-md-2 text-center").html(topic_status))
                    .append($("<td>").addClass("col-md-6 text-center").html(dataInform[i].desciption))
                    .append($("<td>").addClass("col-md-1 text-center").html(date))
                    .append($("<td>").addClass("col-md-1 text-center").html(inform_status).css("color", color))
                )
            j = j+1;
        }
    }
}
// Function : GalleryHome()
// Manage : Get ข้อมูลรูปภาพบ้านลงใน Modal
function GalleryHome() {
    gallery = $.ajax({
        url: "http://localhost:8080/web/galleryHome",
        headers: {
            Accept: "application/json",
        },
        type: "GET",
        async: false,
    }).responseJSON;
    //console.log(gallery)
    var size = 0;
    var j = 1;
    for (var i = 0; i < gallery.length; i++) {
        if(gallery[i].home.id_home == id_home){
            size = size+1;
        }
    }
    //console.log("size img : "+size);
    // เงื่อนไขจัดเตรียมพื้นที่บ้านตามจำนวนรูปบ้านที่มี จัด Form บ้าน
        if(size == 1) {
            $('div[id="galleryHome"]')
                .append($("<div>").addClass("col-sm-12")
                    .append($("<div>").addClass("row").attr("id","img1")
                        .append($("<jsp:text/>"))
                    )
                )
        }
        else if(size == 2) {
            $('div[id="galleryHome"]')
                .append($("<div>").addClass("col-sm-6")
                    .append($("<div>").addClass("row").attr("id","img1")
                        .append($("<jsp:text/>"))
                    )
                )
                .append($("<div>").addClass("col-sm-6")
                    .append($("<div>").addClass("row").attr("id","img2")
                        .append($("<jsp:text/>"))
                    )
                )
        }
        else if(size == 3) {
            $('div[id="galleryHome"]')
                .append($("<div>").addClass("col-sm-8")
                    .append($("<div>").addClass("row").attr("id","img1")
                        .append($("<jsp:text/>"))
                    )
                )
                .append($("<div>").addClass("col-sm-4")
                    .append($("<div>").addClass("row").attr("id","img2")
                        .append($("<jsp:text/>"))
                    )
                    .append($("<div>").addClass("row")
                        .append($("<div>").addClass("col-sm-12")
                            .append($("<div>").addClass("row").attr("id","img3")
                                .append($("<jsp:text/>"))
                            )

                        )
                    )
                )
        }
        else if(size == 4) {
            $('div[id="galleryHome"]')
                .append($("<div>").addClass("col-sm-8")
                    .append($("<div>").addClass("row").attr("id","img1")
                        .append($("<jsp:text/>"))
                    )
                )
                .append($("<div>").addClass("col-sm-4")
                    .append($("<div>").addClass("row").attr("id","img2")
                        .append($("<jsp:text/>"))
                    )
                    .append($("<div>").addClass("row")
                        .append($("<div>").addClass("col-sm-6")
                            .append($("<div>").addClass("row").attr("id","img3")
                                .append($("<jsp:text/>"))
                            )
                        )
                        .append($("<div>").addClass("col-sm-6")
                            .append($("<div>").addClass("row").attr("id","img4")
                                .append($("<jsp:text/>"))
                            )
                        )
                    )
                )
        }
        else if(size > 4) {
            $('div[id="galleryHome"]')
                .append($("<div>").addClass("col-sm-8")
                    .append($("<div>").addClass("row").attr("id","img1")
                        .append($("<jsp:text/>"))
                    )
                )
                .append($("<div>").addClass("col-sm-4")
                    .append($("<div>").addClass("row").attr("id","img2")
                        .append($("<jsp:text/>"))
                    )
                    .append($("<div>").addClass("row")
                        .append($("<div>").addClass("col-sm-6")
                            .append($("<div>").addClass("row").attr("id","img3")
                                .append($("<jsp:text/>"))
                            )
                        )
                        .append($("<div>").addClass("col-sm-6")
                            .append($("<div>").addClass("row").attr("id","img4")
                                .append($("<jsp:text/>"))
                            )
                            .append($("<div>").addClass("more_pic_content").attr("style","display: table; height: 100%; width: 100%;")
                                .append($("<div>").addClass("more_pic_text").attr("data-toggle","modal").attr("data-target","#gallery").attr("style","display: table-cell; text-align: center; vertical-align: middle;")
                                    .append($("<font>").css("font-size", 15).html("รูปภาพทั้งหมด"))
                                )
                            )
                        )
                    )
                )

        }
     //Loop ใส่รูปภาพบ้านเช่าลงใน Modal
    for (var i = 0; i < gallery.length; i++) {
        if(gallery[i].home.id_home == id_home){
            $("#img"+j)
               .append($("<img>").addClass("gallery").attr("src", "http://localhost:8080/web/resources/images/imagesHome/" + gallery[i].img_home + ".png").attr("id","size"+j))

            //Condition push image in Modal
            if(size > 4){
                if(j == 1){
                    $('div[id="inner"]')
                        .append($("<div>").addClass("item active")
                            .append($("<img>").attr("src", "http://localhost:8080/web/resources/images/imagesHome/" + gallery[i].img_home + ".png").attr("style","display: table; height: 340px; width: 778px;"))
                        )
                    $('div[id="inner2"]')
                        .append($("<div>").addClass("item active").attr("id","inner2_1")
                            .append($("<div>").addClass("thumb").attr("data-slide-to",j-1).attr("data-target","#carousel")
                                .append($("<img>").addClass("thumb").attr("src", "http://localhost:8080/web/resources/images/imagesHome/" + gallery[i].img_home + ".png").attr("style","display: table; height: 72px; width: 90px;"))
                            )
                        )
                }else{
                    $('div[id="inner"]')
                        .append($("<div>").addClass("item")
                            .append($("<img>").addClass("item").attr("src", "http://localhost:8080/web/resources/images/imagesHome/" + gallery[i].img_home + ".png").attr("style","display: table; height: 340px; width: 778px;"))
                        )
                    $("#inner2_1")
                        .append($("<div>").addClass("thumb").attr("data-slide-to",j-1).attr("data-target","#carousel")
                            .append($("<img>").addClass("thumb").attr("src", "http://localhost:8080/web/resources/images/imagesHome/" + gallery[i].img_home + ".png").attr("style","display: table; height: 72px; width: 90px;"))
                        )
                }

            }
            j++;
        }
    }
    // เงื่อนไขการวางรูปบ้านลงตามตำแหน่งที่ได้กำหนด Form ไว้ก่อนหน้า
    if(size == 1){
        $('img[id="size1"]')
            .attr("style", "width:100%; height:350px;")
    }
    else if(size == 2){
        $('img[id="size1"]')
            .attr("style", "width:100%; height:350px;")
        $('img[id="size2"]')
            .attr("style", "width:100%; height:350px;")
    }
    else if(size == 3){
        $('img[id="size1"]')
            .attr("style", "width:100%; height:350px;")
        $('img[id="size2"]')
            .attr("style", "width:100%; height:175px;")
        $('img[id="size3"]')
            .attr("style", "width:100%; height:175px;")
    }
    else if(size == 4){
        $('img[id="size1"]')
            .attr("style", "width:100%; height:350px;")
        $('img[id="size2"]')
            .attr("style", "width:100%; height:175px;")
        $('img[id="size3"]')
            .attr("style", "width:100%; height:175px;")
        $('img[id="size4"]')
            .attr("style", "width:100%; height:175px;")
    }
    else if(size > 4){
        $('img[id="size1"]')
            .attr("style", "width:100%; height:350px;")
        $('img[id="size2"]')
            .attr("style", "width:100%; height:175px;")
        $('img[id="size3"]')
            .attr("style", "width:100%; height:175px;")
        $('img[id="size4"]')
            .attr("style", "width:100%; height:175px;")
    }

}
