/*
@author  Sarawut Promsoon
@Create Date 2019-09-20
@Update Date 2019-10-03
* file.js : contract
* Manage : javaScript form v_contract
*/

var i = 0;
var data;
var id_contract;
var id_home;
var index;
var dateOpenPdf;
var dataContract;
$(document).ready(function() {
    // ---menu search---
    $("#all")
        .css("background-color", "#3498DB")
        .css("color", "#FBFCFC");

    $("#all").click(function() {
        $("#all")
            .css("background-color", "#3498DB")
            .css("color", "#FBFCFC");
        $("#book")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
        $("#issue")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
        $("#out")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
    });
    $("#book").click(function() {
        $("#all")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
        $("#book")
            .css("background-color", "#3498DB")
            .css("color", "#FBFCFC");
        $("#issue")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
        $("#out")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
    });
    $("#issue").click(function() {
        $("#all")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
        $("#book")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
        $("#issue")
            .css("background-color", "#3498DB")
            .css("color", "#FBFCFC");
        $("#out")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
    });
    $("#out").click(function() {
        $("#all")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
        $("#book")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
        $("#issue")
            .css("background-color", "#F7F9F9")
            .css("color", "#17202A");
        $("#out")
            .css("background-color", "#3498DB")
            .css("color", "#FBFCFC");
    });
    // get id home From URL
    var full_url = document.URL;
    var url_array = full_url.split('=')
    id_home = url_array[url_array.length-1];
    getDataBook();
    showData();
})
function getDataBook() {
     data = $.ajax({
        url: "http://localhost:8080/web/getDataBook",
        headers:{
            Accept:"application/json"
        },
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        type:"GET",
        async:false,
        data: JSON.stringify(),
        complete:function (xhr) {

        }
    }).responseJSON;
}
function shadow(x) {
    x.classList.add("shadow");
}
function noShadow(x) {
    x.classList.remove("shadow");
}
function showData() {
    var now = new Date();
    var today = formatDate2(now);
    console.log(data)
    for(var i = 0 ; i < data.length;i++){
        if(id_home == data[i].home.id_home ){
            if(data[i].status_book != 0){
                index = i;
                var numberTime = parseInt(data[i].date_book);
                var dayBook = formatDate1(numberTime);
                $("#nameHome").text("ชื่อบ้าน "+data[i].home.homeAddress.nameHome)

                $("#dataHome1").text("ขนาดบ้าน "+data[i].home.house_size+" ตารางวา จำนวนชั้น "+data[i].home.number_floors+" ชั้น ขนาดห้อง "+data[i].home.room_size+" ตารางวา "+data[i].home.number_room+" ห้องนอน "+data[i].home.number_toilet+" ห้องน้ำ");
                $("#dataHome2").text("ค่าน้ำฝหน่วย "+data[i].home.cost_water+" บาท ค่าไฟ/หน่วย "+data[i].home.cost_fire+" บาท");
                $("#dataHome3").text("ค่าเช่า/เดือน "+data[i].home.cost_hire+" บาท");
                $("#dataAddress1").text("เลขที่บ้าน "+data[i].home.homeAddress.numberHome+" หมู่ "+data[i].home.homeAddress.villageNo+" หมู่บ้าน "+data[i].home.homeAddress.village+" ตรอก/ซอย "+data[i].home.homeAddress.alley);
                $("#dataAddress2").text("ถนน "+data[i].home.homeAddress.road+" ตำบล/แขวง "+data[i].home.homeAddress.district+" อำเภอ/เขต "+data[i].home.homeAddress.prefecture);
                $("#dataAddress3").text(" จังหวัด "+data[i].home.homeAddress.county+" รหัสไปรษณีย์ "+data[i].home.homeAddress.post);
                $("#fullName").text(data[i].home.user.fullName);
                $("#phone").text(data[i].home.user.phone);
                $("#email").text(data[i].home.user.email);
                $("#lineId").text(data[i].home.user.line_id);

                $("#dateBook").text(dayBook);
                $("#nameUser").text(data[i].user.fullName);
                $("#phoneUser").text(data[i].user.phone);
                $("#codeName").text(data[i].user.id_user);
            }
            if(data[i].home.status_home == 3){
                    if (id_home == data[i].home.id_home && data[i].contract != null) {
                        if(data[i].contract.status_contract == 1){
                            dateOpenPdf = data[i].contract.date_start;
                            var date_start = formatDate2(data[i].contract.date_start);
                            var date_end = formatDate2(data[i].contract.date_end);
                            id_contract = data[i].contract.id_contract;
                            $("#dateStart").attr("disabled","true").val(date_start);
                            $("#dateEnd").attr("disabled","true").val(date_end);
                            $("#lengthDate").attr("disabled","true").val(data[i].contract.phase_time);
                            $("#rent").attr("disabled","true").val(data[i].contract.cost_hire);
                            $("#bail").attr("disabled","true").val(data[i].contract.bail);
                            $("#save").attr("disabled","true");
                            $("#print1").removeAttr("disabled").attr("onclick","OpenPdf()");
                            $("#print2").removeAttr("disabled").attr("onclick","OpenInvoivePdf()");
                            $("#buttonManage").empty();
                            $("#buttonManage").append($("<a>").attr("id","cancel").addClass("btn btn-danger btn-block btn-lg").attr("onmouseover","shadow(this)").attr("onmouseout","noShadow(this)").attr("onclick","cancelContract()").text("ยกเลิก"))

                        }
                    }
            }else{
                $("#rent").val(data[i].home.cost_hire);
                $("#dateStart").val(today);
            }
        }
    }
}

function printContract() {
    var today = DateFormatToday();
    var date = formatDate4() ;
    var dataContract = {
        "idUser":data[index].user.id_user,
        "idHome":data[index].home.id_home,
        "lengthDate":$("#lengthDate").val(),
        "dateStart":$("#dateStart").val(),
        "dateEnd":$("#dateEnd").val(),
        "rent":$("#rent").val(),
        "bail":$("#bail").val(),
        "today":today,
        "R_user":data[index].user.fullName,
        "R_age":data[index].user.age,
        "R_number_home":data[index].user.user_address.number_home,
        "R_alley":data[index].user.user_address.alley,
        "R_road":data[index].user.user_address.road,
        "R_district":data[index].user.user_address.district,
        "R_prefecture":data[index].user.user_address.prefecture,
        "R_county":data[index].user.user_address.county,
        "O_user":data[index].home.user.fullName,
        "O_age":data[index].home.user.age,
        "O_number_home":data[index].home.user.user_address.number_home,
        "O_alley":data[index].home.user.user_address.alley,
        "O_road":data[index].home.user.user_address.road,
        "O_district":data[index].home.user.user_address.district,
        "O_prefecture":data[index].home.user.user_address.prefecture,
        "O_county":data[index].home.user.user_address.county,
        "home_number":data[index].home.homeAddress.numberHome,
        "home_alley":data[index].home.homeAddress.alley,
        "home_road":data[index].home.homeAddress.road,
        "home_district":data[index].home.homeAddress.district,
        "home_prefecture":data[index].home.homeAddress.prefecture,
        "home_county":data[index].home.homeAddress.county,
        "id_book":data[index].id_book,
        "namePdf":"H"+data[index].home.id_home+"U"+data[index].user.id_user+"_report"+date,
        "name_ReceiptPdf":"H"+data[index].home.id_home+"U"+data[index].user.id_user+"_receipt"+date,
        "cost_fire":data[index].home.cost_fire,
        "cost_water":data[index].home.cost_water,
        "cost_hire":data[index].home.cost_hire,
        "home_name":data[index].home.homeAddress.nameHome
    }
    $.ajax({
        url: "http://localhost:8080/web/getpdf",
        headers:{
            Accept:"application/json",
        },
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        type:"POST",
        async:false,
        data: JSON.stringify(dataContract),
        complete:function (xhr) {
        }
    })
    $.ajax({
        url: "http://localhost:8080/web/getInvoivePdf",
        headers:{
            Accept:"application/json",
        },
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        type:"POST",
        async:false,
        data: JSON.stringify(dataContract),
        complete:function (xhr) {
        }
    })
}
function DateFormatToday() {
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var date = now.getFullYear()+"/"+(month)+"/"+(day) ;
    return date;
}
function formatDate1(numberTime){
    var timestamp = new Date(numberTime);
    var d = timestamp.getDate().toString();
    var dd = (d.length === 2) ? d : "0"+d;
    var m = (timestamp.getMonth()+1).toString();
    var mm = (m.length === 2) ? m : "0"+m;
    return (dd+"/"+mm+ "/" + (timestamp.getFullYear()).toString());
}
function formatDate2(numberTime){
    var now = new Date(numberTime);
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var date = now.getFullYear()+"-"+(month)+"-"+(day) ;
    return date;
}
function formatDate3(numberTime) {
    var now = new Date(numberTime);
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var date = now.getFullYear()+"_"+(month)+"_"+(day) ;
    return date;
}
function formatDate4() {
    var now = new Date();
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);
    var date = now.getFullYear()+"_"+(month)+"_"+(day) ;
    return date;
}
function saveContract(){
    var date = formatDate4() ;
    var dataContract = {
        "namePdf":"H"+data[index].home.id_home+"U"+data[index].user.id_user+"_report"+date,
        "name_ReceiptPdf":"H"+data[index].home.id_home+"U"+data[index].user.id_user+"_receipt"+date,
        "id_home":data[index].home.id_home,
        "id_book":data[index].id_book,
        "dateStart":$("#dateStart").val(),
        "dateEnd":$("#dateEnd").val(),
        "lengthDate":$("#lengthDate").val(),
        "rent":$("#rent").val(),
        "bail":$("#bail").val(),
    }
    $.ajax({
        url: "http://localhost:8080/web/saveContract",
        headers:{
            Accept:"application/json",
        },
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        type:"POST",
        async:false,
        data: JSON.stringify(dataContract),
        complete:function (xhr) {

        }
    })
}
function cancelContract() {
    Swal.fire({
        title: 'ต้องการยกเลิกจริงหรือไม่?',
        text: "เมื่อยกเลิกเล้วจะไม่สามารถแก้ไขได้อีก!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ใช่',
        cancelButtonText: 'ไม่'
    }).then((result) => {
        if (result.value) {
        Swal.fire({
            position: 'top-end',
            type: 'success',
            title: 'ยกเลิกสำเร็จ',
            showConfirmButton: false,
            timer: 1500
        })
        var dataDelete = {
            "id_home": data[index].home.id_home ,
            "id_book": data[index].id_book,
            "id_contract": id_contract,
        }
        $.ajax({
            url: "http://localhost:8080/web/deleteContract",
            headers:{
                Accept:"application/json",
            },
            contentType:"application/json; charset=utf-8",
            dataType:"json",
            type:"POST",
            async:false,
            data: JSON.stringify(dataDelete),
            complete:function (xhr) {

            }
        })
        var link = document.createElement("a");
        link.href = "http://localhost:8080/web/own_manage";
        link.click();
    }

})
    //

}

function OpenPdf() {
    var date = formatDate3(data[index].contract.date_start) ;

    var link = document.createElement("a");
    link.download = "H"+data[index].home.id_home+"U"+data[index].user.id_user+"_report"+date+".pdf";
    link.href = "http://localhost:8080/web/resources/reports/contractPDF/H"+data[index].home.id_home+"U"+data[index].user.id_user+"_report"+date+".pdf";
    link.click();
}
function OpenInvoivePdf() {
    var date = formatDate3(data[index].contract.date_start) ;
    var link = document.createElement("a");
    link.download = "H"+data[index].home.id_home+"U"+data[index].user.id_user+"_receipt"+date+".pdf";
    link.href = "http://localhost:8080/web/resources/reports/invoivePDF/H"+data[index].home.id_home+"U"+data[index].user.id_user+"_receipt"+date+".pdf";
    link.click();
}

function validate() {
    var dateStart = $("#dateStart").val();
    var lengthDate = $("#lengthDate").val();
    var dateEnd = $("#dateEnd").val();
    var rent = $("#rent").val();
    var bail = $("#bail").val();
    console.log(dateStart);

    if(dateStart == ""){
        $("#dateStart").addClass("classCheck");
    }else{
        $("#dateStart").removeClass("classCheck")
    }
    if(lengthDate == ""){
        $("#lengthDate").addClass("classCheck");
    }else{
        $("#lengthDate").removeClass("classCheck")
    }
    if(dateEnd == ""){
        $("#dateEnd").addClass("classCheck");
    }else{
        $("#dateEnd").removeClass("classCheck")
    }
    if(rent == ""){
        $("#rent").addClass("classCheck");
    }else{
        $("#rent").removeClass("classCheck")
    }
    if(bail == ""){
        $("#bail").addClass("classCheck");
    }else{
        $("#bail").removeClass("classCheck")
    }

    if(dateStart != ""&& lengthDate != "" && dateEnd != "" && rent != "" && bail != "" ){
        return true;
    }else{
        return false;
    }

}
function checkSave() {
    if(validate()){
        Swal.fire({
            title: 'ต้องการบันทึกจริงหรือไม่?',
            text: "เมื่อบันทึกเเล้วจะไม่สามารถแก้ไขได้อีก!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ใช่',
            cancelButtonText: 'ไม่'
        }).then((result) => {
            if (result.value) {
            saveContract();
            Swal.fire({
                position: 'top-end',
                type: 'success',
                title: 'บันทึกสำเร็จ',
                showConfirmButton: false,
                timer: 1500
            })
            printContract();
            getDataBook();
            showData();
            }
        })

    }else{
        Swal.fire(
            'ไม่สามารถบันทึกได้!',
            'กรุณาใส่ข้อมูลให้ครบ',
            'warning'
        )
    }
}