// @author  Natthakit  Poltirach
// @Create Date  2019-09-11
// @Update Date  -
var id_edit_user;
$(document).ready(function(){
    $('button[id="updateData"]').click(function(){
        EnableData();
    });
    //Get ข้อมูล User ตาม id_user
    var data = $.ajax({
        url: "http://localhost:8080/web/profile?id=2",
        headers:{
            Accept:"application/json",
        },
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        type:"GET",
        async:false,
        data: JSON.stringify()
    }).responseJSON;
    //console.log(data);
    id_edit_user = data.id_user;
    var calendar = new Date(data.birthday);
    var day = ("0" + calendar.getDate()).slice(-2);
    var month = ("0" + (calendar.getMonth() + 1)).slice(-2);
    var date = calendar.getFullYear()+"-"+(month)+"-"+(day) ;
    $('input[id="fullName"]').val(data.fullName);
    $('input[id="birth"]').val(date);
    $('input[id="age"]').val(data.age);
    $('input[id="email"]').val(data.email);
    $('input[id="phone"]').val(data.phone);
    $('input[id="name_home"]').val(data.user_address.name_home);
    $('input[id="number_home"]').val(data.user_address.number_home);
    $('input[id="village_no"]').val(data.user_address.village_no );
    $('input[id="village"]').val(data.user_address.village);
    $('input[id="alley"]').val(data.user_address.alley);
    $('input[id="road"]').val(data.user_address.road);
    $('input[id="district"]').val(data.user_address.district);
    $('input[id="prefecture"]').val(data.user_address.prefecture);
    $('input[id="county"]').val(data.user_address.county);
    $('input[id="post"]').val(data.user_address.post);
});
//upload รูปภาพ
$(function() {
    $(document).on("change",".uploadFile", function()
    {
        var uploadFile = $(this);
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return; // no file selected, or no FileReader support
        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file

            reader.onloadend = function(){ // set image data as background of div
                //alert(uploadFile.closest(".upimage").find('.imagePreview').length);
                uploadFile.closest(".panel-body").find('.imagePreview').css("background-image", "url("+this.result+")");
            }
        }
    });
});
// Function : UpdateProfile()
// Manage : Post ข้อมูล User ไปยัง Backend และทำการ Set ข้อมูล User ที่ id_User เดิม และมีเงื่อนไขในการ Update ข้อมูล User
function UpdateProfile(){
    var fullName = "";
    var age = "";
    var birthday = "";
    var email = "";
    var phone = "";
    var post = "";
    var number_home = "";
    var alley = "";
    var county = "";
    var district = "";
    var prefecture = "";
    var road = "";
    var name_home = "";
    var village = "";
    var village_no = "";
        fullName = $('input[id="fullName"]').val();
        age = $('input[id="age"]').val();
        birthday = $('input[id="birth"]').val();
        phone = $('input[id="phone"]').val();
        email = $('input[id="email"]').val();
        name_home = $('input[id="name_home"]').val();
        number_home = $('input[id="number_home"]').val();
        village_no = $('input[id="village_no"]').val();
        village = $('input[id="village"]').val();
        alley = $('input[id="alley"]').val();
        road = $('input[id="road"]').val();
        district = $('input[id="district"]').val();
        prefecture = $('input[id="prefecture"]').val();
        county = $('input[id="county"]').val();
        post = $('input[id="post"]').val();
    if($('input[id="fullName"]').val() != "" && $('input[id="age"]').val() != "" && $('input[id="birth"]').val() != "" && $('input[id="phone"]').val() != "" && $('input[id="email"]').val() != "" && $('input[id="name_home"]').val() != "" && $('input[id="number_home"]').val() != "" && $('input[id="village_no"]').val() != "" && $('input[id="village"]').val() != "" && $('input[id="alley"]').val() != "" && $('input[id="road"]').val() != "" && $('input[id="district"]').val() != "" && $('input[id="prefecture"]').val() != "" && $('input[id="county"]').val() != "" && $('input[id="post"]').val() != "") {
        var Profile = {
            "id_edit_user": "" + id_edit_user,
            "full_name": fullName,
            "age": age,
            "birthday": birthday,
            "phone": phone,
            "email": email,
            "post": post,
            "number_home": number_home,
            "alley": alley,
            "county": county,
            "district": district,
            "prefecture": prefecture,
            "road": road,
            "name_home": name_home,
            "village": village,
            "village_no": village_no
        }
        //console.log("Profile : " + Profile);
        $.ajax({
            url: "http://localhost:8080/web/updateProfile",
            headers: {
                Accept: "application/json",
            },
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            type: "POST",
            async: false,
            data: JSON.stringify(Profile),
            complete: function (xhr) {
            }
        })
        Swal.fire({
            position: 'center',
            type: 'success',
            title: 'แก้ไขข้อมูลเสร็จสิ้น',
            showConfirmButton: false,
            timer: 2000
        })
        DisableData();
    }
    else{
        Swal.fire({
            type: 'info',
            title: 'เกิดข้อผิดพลาด',
            text: 'กรุณากรอกข้อมูลให้ครบถ้วน!',
        })
        EnableData();
    }
}
// Function : EnableData()
// Manage : ปรับให้ตำแหน่งที่กำหนดเป็น Disabled เป็น Enable
function EnableData(){
    $('input[id="fullName"]').removeAttr('disabled');
    $('input[id="age"]').removeAttr('disabled');
    $('input[id="birth"]').removeAttr('disabled');
    $('textarea[id="location"]').removeAttr('disabled');
    $('input[id="phone"]').removeAttr('disabled');
    $('input[id="email"]').removeAttr('disabled');
    $('button[id="update"]').removeAttr('disabled');
    $('label[id="upload"]').removeAttr('disabled');
    $('input[id="imagePreview"]').removeAttr('disabled');
    $('input[id="post"]').removeAttr('disabled');
    $('input[id="number_home"]').removeAttr('disabled');
    $('input[id="alley"]').removeAttr('disabled');
    $('input[id="county"]').removeAttr('disabled');
    $('input[id="district"]').removeAttr('disabled');
    $('input[id="prefecture"]').removeAttr('disabled');
    $('input[id="road"]').removeAttr('disabled');
    $('input[id="name_home"]').removeAttr('disabled');
    $('input[id="village"]').removeAttr('disabled');
    $('input[id="village_no"]').removeAttr('disabled');
}
// Function : EDisableData()
// Manage : กำหนดตำแหน่งให้เป็น Disabled
function DisableData(){
    $('input[id="fullName"]').attr('disabled' ,'true');
    $('input[id="age"]').attr('disabled' ,'true');
    $('input[id="birth"]').attr('disabled' ,'true');
    $('input[id="location"]').attr('disabled' ,'true');
    $('textarea[id="location"]').attr('disabled' ,'true');
    $('input[id="phone"]').attr('disabled' ,'true');
    $('input[id="email"]').attr('disabled' ,'true');
    $('button[id="update"]').attr('disabled' ,'true');
    $('label[id="upload"]').attr('disabled' ,'true');
    $('input[id="imagePreview"]').attr('disabled' ,'true');
    $('input[id="post"]').attr('disabled' ,'true');
    $('input[id="number_home"]').attr('disabled' ,'true');
    $('input[id="alley"]').attr('disabled' ,'true');
    $('input[id="county"]').attr('disabled' ,'true');
    $('input[id="district"]').attr('disabled' ,'true');
    $('input[id="prefecture"]').attr('disabled' ,'true');
    $('input[id="road"]').attr('disabled' ,'true');
    $('input[id="name_home"]').attr('disabled' ,'true');
    $('input[id="village"]').attr('disabled' ,'true');
    $('input[id="village_no"]').attr('disabled' ,'true');
}

