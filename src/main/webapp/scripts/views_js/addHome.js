/*
@author  Sarawut Promsoon
@Create Date 2019-09-09
@Update Date 2019-10-03
* file.js : addHome
* Manage : javaScript form v_add_home
*/

var county = [
    "ChiangMai",
    "ChiangRai",
    "Lampang",
    "Lamphun",
    "MaeHongSon",
    "Nan",
    "Phayao",
    "Phrae",
    "Uttaradit",
    "Kalasin ",
    "KhonKaen",
    "Chaiyaphum",
    "NakhonPhanom",
    "NakhonRatchasima",
    "BuengKan",
    "Buriram",
    "MahaSarakham",
    "Mukdahan",
    "Yasothon",
    "RoiEt",
    "Loei",
    "SakonNakhon",
    "Surin",
    "Sisaket",
    "NongKhai",
    "NongBuaLamphu",
    "UdonThani",
    "UbonRatchathani",
    "AmnatCharoen",
    "Bangkok",
    "KamphaengPhet",
    "ChaiNat",
    "NakhonNayok",
    "NakhonPathom",
    "NakhonSawan",
    "Nonthaburi",
    "PathumThani",
    "PhraNakhonSiAyutthaya",
    "Phichit",
    "Phitsanulok",
    "Phetchabun",
    "Lopburi",
    "SamutPrakan",
    "SamutSongkhram",
    "SamutSakhon",
    "SingBuri",
    "Sukhothai",
    "SuphanBuri",
    "Saraburi",
    "AngThong",
    "UthaiThani",
    "Chanthaburi",
    "Chachoengsao",
    "Chonburi",
    "Trat",
    "Prachinburi",
    "Rayong",
    "SaKaeo",
    "Kanchanaburi",
    "Tak",
    "PrachuapKhiriKhan",
    "Phetchaburi",
    "Ratchaburi",
    "Krabi",
    "Chumphon",
    "Trang",
    "NakhonSiThammarat",
    "Narathiwat",
    "Pattani",
    "PhangNga",
    "Phatthalung",
    "Phuket",
    "Ranong",
    "Satun",
    "Songkhla",
    "SuratThani",
    "Yala"
];
var size = county.length;
var i = 0;
var size_bank ;
var iCheck = 0;
var id_user = 1;
var checkImg = 0;
$(document).ready(function() {
    //=== set data County in drop down ===
    setSeleteCounty();

    //=== add input Bank When click button ===
    $("#pud").click(function() {
        addInputBank();
    });

    //============== Checked validate one by one ==================
    $("#home_name").change(function () {
        checkOne1("home_name");
    })
    $("#home_number").change(function () {
        checkOne1("home_number");
    })
    $("#village_no").change(function () {
        checkOne1("village_no");
    })
    $("#village").change(function () {
        checkOne1("village");
    })
    $("#alley").change(function () {
        checkOne1("alley");
    })
    $("#road").change(function () {
        checkOne1("road");
    })
    $("#district").change(function () {
        checkOne1("district");
    })
    $("#district_area").change(function () {
        checkOne1("district_area");
    })
    $("#county").change(function () {
        checkOne2("county");
    })
    $("#zipCode").change(function () {
        checkOne1("zipCode");
        if(isNaN($("#zipCode").val())){
            Swal.fire(
                'ใส่ได้เฉพาะตัวเลข!',
                'กรุณาใส่ข้อมูลให้ถูกต้อง',
                'warning'
            )
            $("#zipCode").addClass("classCheck");
        }
    })
    $("#sizeHome").change(function () {
        checkOne1("sizeHome");
    })
    $("#number_floors").change(function () {
        checkOne1("number_floors");
    })
    $("#sizeRoom").change(function () {
        checkOne1("sizeRoom");
    })
    $("#number_room").change(function () {
        checkOne1("number_room");
    })
    $("#number_toilet").change(function () {
        checkOne1("number_toilet");
    })
    $("#water").change(function () {
        checkOne1("water");
    })
    $("#electricity").change(function () {
        checkOne1("electricity");
    })
    $("#month").change(function () {
        checkOne1("month");
    })
    $("#bank0").change(function () {
        checkOne2("bank0");
    })
    $("#bank_number0").change(function () {
        checkOne1("bank_number0");
        if(isNaN($("#bank_number0").val())){
            Swal.fire(
                'ใส่ได้เฉพาะตัวเลข!',
                'กรุณาใส่ข้อมูลให้ถูกต้อง',
                'warning'
            )
            $("#bank_number0").addClass("classCheck");
        }
    })

    $('#multipleUploadForm').submit(function(event) {
        console.log("55:"+$('#multipleFileUploadInput'));
        var formElement = this;
        var formData = new FormData(formElement);
        console.log(formElement);
        console.log("=====");
        console.log(formData);
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "http://localhost:8080/web/uploadMultipleFiles?id=U"+id_user+"H"+$("#home_name").val(),
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                console.log(response);
                // process response
            },
            error: function (error) {
                console.log(error);
                // process error
            }
        });
        event.preventDefault();
        $("#uploadConnect").text("Upload complete").css("color","green");
        checkImg = 1;
    });


});

function shadow(x) {
    x.classList.add("shadow");
}

function noShadow(x) {
    x.classList.remove("shadow");
}
function RemoveCreate(id){
    var tr_create = document.getElementById("tr_"+id);
    tr_create.parentNode.removeChild(tr_create);
}
function setSeleteCounty() {
    for (var c = 0; c < size; c++) {
        if (c == 0) {
            $("#county").append('<option value="0">--เลือก--</option>');
        }
        $("#county").append(
            '<option value="' + county[c] + '">' + county[c] + "</option>"
        );
    }
}
function addInputBank() {
    i=i+1;
    $("tbody").append($("<tr>").attr("id","tr_"+i)
        .append($("<td>").addClass("col-sm-1")
            .append($("<div>").addClass("row").text("ธนาคาร:")
                .append($("<span>").append("*"))
            )
        )
        .append($("<td>").addClass("col-sm-3")
            .append($("<div>").addClass("col-sm-10")
                .append($("<select>").addClass("form-control").attr("id","bank"+i)
                    .append($("<option>").attr("value","").attr("disabled","ture").attr("selected","ture").text("-- เลือก --"))
                    .append($("<option>").attr("value","BBL").text("ธ. กรุงเทพ"))
                    .append($("<option>").attr("value","KBANK").text("ธ. กสิกรไทย"))
                    .append($("<option>").attr("value","KTB").text("ธ. กรุงไทย"))
                    .append($("<option>").attr("value","BAY").text("ธ. กรุงศรีอยุธยา"))
                    .append($("<option>").attr("value","SCB").text("ธ. ไทยพาณิชย์"))
                )
            )
        )
        .append($("<td>").addClass("col-sm-1")
            .append($("<div>").addClass("row").text("เลขที่บัญชี:")
                .append($("<span>")
                    .append("*")
                )
            )
        )
        .append($("<td>").addClass("col-sm-2")
            .append($("<input>").addClass("form-control").attr("placeholder", "xxx-xxxxxx-x").attr("maxlength", "10").attr("type", "text").attr("id","bank_number"+i)
            )
        )
        .append($("<td>")
            .append($("<button>").addClass("btn btn-danger dele").attr("type","button").attr("onclick",'RemoveCreate("'+i+'")')
                .append($("<i>").addClass("glyphicon glyphicon-remove")
                )
            )
        )
    );
    size_bank = i;

}
function addDataHome() {
    if(validate()){
        var bank = [];
        for(var i = 0; i< document.getElementsByTagName("tr").length ;i++){
            bank.push({
                "bank": document.getElementsByTagName("tr")[i].getElementsByTagName("td")[1].getElementsByTagName("div")[0].getElementsByTagName("select")[0].value,
                "number_bank":document.getElementsByTagName("tr")[i].getElementsByTagName("td")[3].getElementsByTagName("input")[0].value
            })
        }
        console.log(bank)
        console.log("json: "+bank.length);
        var home = {
            "home_name":$("#home_name").val(),
            "home_number":$("#home_number").val(),
            "village_no":$("#village_no").val(),
            "village":$("#village").val(),
            "alley":$("#alley").val(),
            "road":$("#road").val(),
            "district":$("#district").val(),
            "district_area":$("#district_area").val(),
            "county":$("#county").val(),
            "zipCode":$("#zipCode").val(),

            "img": "U"+id_user,
            "sizeHome":$("#sizeHome").val(),
            "number_floors":$("#number_floors").val(),
            "sizeRoom":$("#sizeRoom").val(),
            "number_room":$("#number_room").val(),
            "number_toilet":$("#number_toilet").val(),
            "water":$("#water").val(),
            "electricity":$("#electricity").val(),
            "month":$("#month").val(),
            "bank":bank,
            "size_bank":bank.length,
        }
        // var encode = encodeURI(home);
        $.ajax({
            url: "http://localhost:8080/web/datahome",
            headers:{
                Accept:"application/json",
            },
            contentType:"application/json; charset=utf-8",
            dataType:"json",
            type:"POST",
            //async:false,
            data: JSON.stringify(home),
            //complete:function (xhr) {
            //}
        })
        Swal.fire({
            position: 'top-end',
            type: 'success',
            title: 'บันทึกสำเร็จ',
            showConfirmButton: false,
            timer: 1500
        })
        setTimeout(clickLink,2000)
    }else{
        Swal.fire(
            'ไม่สามารถบันทึกได้!',
            'กรุณาใส่ข้อมูลให้ครบ',
            'warning'
        )
    }
}
function clickLink() {
    var link = document.createElement("a");
    link.href = "http://localhost:8080/web/own_manage";
    link.click();
}
function validate() {
    var home_name = $("#home_name").val();
    var home_number = $("#home_number").val();
    var village_no = $("#village_no").val();
    var village = $("#village").val();
    var alley = $("#alley").val();
    var road =  $("#road").val();
    var district = $("#district").val();
    var district_area = $("#district_area").val();
    var county = $("#county").val();
    var zipCode = $("#zipCode").val();

    var sizeHome = $("#sizeHome").val();
    var number_floors = $("#number_floors").val();
    var sizeRoom = $("#sizeRoom").val();
    var number_room = $("#number_room").val();
    var number_toilet = $("#number_toilet").val();
    var water = $("#water").val();
    var electricity = $("#electricity").val();
    var month = $("#month").val();

    var bank0 = $("#bank0").val();
    var bank_number0 = $("#bank_number0").val();
    var checkBank;

    var checkUpload = $("#multipleFileUploadInput").val();

    if(home_name == ""){
        $("#home_name").addClass("classCheck");
    }else{
        $("#home_name").removeClass("classCheck")
    }
    if(home_number == ""){
        $("#home_number").addClass("classCheck");
    }else{
        $("#home_number").removeClass("classCheck")
    }
    if(village_no == ""){
        $("#village_no").addClass("classCheck");
    }else{
        $("#village_no").removeClass("classCheck")
    }
    if(village == ""){
        $("#village").addClass("classCheck");
    }else{
        $("#village").removeClass("classCheck")
    }
    if(alley == ""){
        $("#alley").addClass("classCheck");
    }else{
        $("#alley").removeClass("classCheck")
    }
    if(road == ""){
        $("#road").addClass("classCheck");
    }else{
        $("#road").removeClass("classCheck")
    }
    if(district == ""){
        $("#district").addClass("classCheck");
    }else{
        $("#district").removeClass("classCheck")
    }
    if(district_area == ""){
        $("#district_area").addClass("classCheck");
    }else{
        $("#district_area").removeClass("classCheck")
    }
    if(county == 0){
        $("#county").addClass("classCheck");
    }else{
        $("#county").removeClass("classCheck")
    }
    if(zipCode == ""){
        $("#zipCode").addClass("classCheck");
    }else{
        $("#zipCode").removeClass("classCheck")
    }
    //===========================
    if(sizeHome == ""){
        $("#sizeHome").addClass("classCheck");
    }else{
        $("#sizeHome").removeClass("classCheck")
    }
    if(number_floors == ""){
        $("#number_floors").addClass("classCheck");
    }else{
        $("#number_floors").removeClass("classCheck")
    }
    if(sizeRoom == ""){
        $("#sizeRoom").addClass("classCheck");
    }else{
        $("#sizeRoom").removeClass("classCheck")
    }
    if(number_room == ""){
        $("#number_room").addClass("classCheck");
    }else{
        $("#number_room").removeClass("classCheck")
    }
    if(number_toilet == ""){
        $("#number_toilet").addClass("classCheck");
    }else{
        $("#number_toilet").removeClass("classCheck")
    }
    if(water == ""){
        $("#water").addClass("classCheck");
    }else{
        $("#water").removeClass("classCheck")
    }
    if(electricity == ""){
        $("#electricity").addClass("classCheck");
    }else{
        $("#electricity").removeClass("classCheck")
    }
    if(month == ""){
        $("#month").addClass("classCheck");
    }else{
        $("#month").removeClass("classCheck")
    }

    if(bank0 == 0){
        $("#bank0").addClass("classCheck");
    }else{
        $("#bank0").removeClass("classCheck")
    }
    if(bank_number0 == ""){
        $("#bank_number0").addClass("classCheck");
    }else{
        $("#bank_number0").removeClass("classCheck")
    }
    for(var i = 0; i< document.getElementsByTagName("tr").length ;i++){
        var data1 = document.getElementsByTagName("tr")[i].getElementsByTagName("td")[1].getElementsByTagName("div")[0].getElementsByTagName("select")[0].value
        var data2 = document.getElementsByTagName("tr")[i].getElementsByTagName("td")[3].getElementsByTagName("input")[0].value
        if(data1 != "" && data2 !=""){
            checkBank = "1";
        }else{
            checkBank = "";
            break;
        }
    }
    if(home_name != ""&& home_number != "" && village_no != "" && village != "" && alley != "" && road != "" && district != "" && district_area != "" && county != 0 && zipCode != "" && sizeHome != "" && number_floors != "" && sizeRoom != "" && number_room != "" && number_toilet != "" && water != "" && electricity != "" && month != "" && checkUpload != "" && bank0 != 0 && checkImg != 0 && checkBank != ""){
        return true;
    }else{
        return false;
    }
}
function checkOne1(name) {
    if($("#"+name).val() == ""){
        $("#"+name).addClass("classCheck");
    }else{
        $("#"+name).removeClass("classCheck")
    }
}
function checkOne2(name) {
    if($("#"+name).val() == 0){
        $("#"+name).addClass("classCheck");
    }else{
        $("#"+name).removeClass("classCheck")
    }
}