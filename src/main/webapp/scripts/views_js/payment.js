//  @Author Natthakit Poltirach
//  @Create Date  2019-09-23
//  @Update Date  2019-10-10
var full_url = document.URL; // Get current url
var url_array = full_url.split('=') // Split the string into an array with / as separator
var id_home = url_array[url_array.length - 1];  // Get the last part of the array (-1)
var i = 0;
var data;
var count = 1;
var date = Date.now();
var calendar = new Date(date);
var day = ("0" + calendar.getDate()).slice(-2);
var month = ("0" + (calendar.getMonth() + 1)).slice(-2);
var today = calendar.getFullYear()+"-"+(month)+"-"+(day) ;
var date_book = today;
$(document).ready(function(){
    btnCancelReserv();
    TotalCost();
    $('#multipleUploadForm').submit(function(event) {

        var formElement = this;
        // You can directly create form data from the form element
        // (Or you could get the files from input element and append them to FormData as we did in vanilla javascript)
        var formData = new FormData(formElement);
      //  console.log(formElement);
      //  console.log(formData);
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "http://localhost:8080/web/uploadMultipleFiles?id=U2B"+date_book,
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                console.log(response);
            },
            error: function (error) {
                console.log(error);
            }
        });
        event.preventDefault();
        $("#uploadConnect").text("Upload complete").css("color","green");

    });
    //จะสามารถกดปุ่มยืนยันได้ก็ต่อเมื่อมีการ Upload ไฟล์รูปภาพ png
    $('button[class="primary submit-btn"]').click(function(){
        if($('input[class="file-input"]').val() != ""){
            SaveBill();
            $('button[id="request"]').removeAttr('disabled');
        }
    });
});
// Function : Books()
// Manage : ส่งค่าไปยัง Backend เพื่อที่จะจองบ้าน
function Books() {
    var bill_book = "U2B"+date_book;
    var status_book = 1;
    var Books = {
        "date_book":date_book,
        "bill_book":bill_book,
        "status_book":status_book,
        "id_home":id_home,
        "id_user":2,
    }
//console.log(date_book)
$.ajax({
        url: "http://localhost:8080/web/books",
        headers:{
            Accept:"application/json",
        },
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        type:"POST",
        async:false,
        data: JSON.stringify(Books),
        complete:function (xhr) {
        }
    })
    Swal.fire({
        position: 'center',
        type: 'success',
        title: 'การจองเสร็จสิ้น',
        showConfirmButton: false,
        timer: 2000
    }).then((result) => {
        if (result.value) {
            var link = document.createElement("a");
            link.href = "http://localhost:8080/web/renter_manage";
            link.click();
        } else{
            link = document.createElement("a");
            link.href = "http://localhost:8080/web/renter_manage";
            link.click();
        }
    });
}
// Function : btnCancelReserv()
// Manage : สร้างปุ่ม "ยกเลิก" เมื่อทำการกดให้ย้อนไปหน้ารายละเอียดบ้านที่ต้องการจะเช่า
function TotalCost(){
    data = $.ajax({
        url: "http://localhost:8080/web/reservDetailHome?id=" + id_home,
        headers: {
            Accept: "application/json",
        },
        type: "GET",
        async: false,
    }).responseJSON;
   // console.log(data);
    var total = data.cost_hire / 2;
    $('div[id="list-home"]')
        .append($("<div>").addClass("row")
            .append($("<div>").addClass("col-sm-1").attr("style","text-align: center;")
                .html(count++)

            )
            .append($("<div>").addClass("col-sm-2")
                .append($("<img>").attr("src", "http://localhost:8080/web/resources/images/imagesHome/" + data.img_home[0].img_home + ".png").attr("style","width: 100%; height: 100%;"))
            )
            .append($("<div>").addClass("col-sm-7")
                .append($("<div>").html("ชื่อบ้าน : " + data.homeAddress.nameHome + "&emsp;เลขที่บ้าน : " + data.homeAddress.numberHome + "&emsp;หมู่ที่ : " + data.homeAddress.villageNo + "&emsp;หมู่บ้าน : " + data.homeAddress.village))
                .append($("<div>").html("ตรอก/ซอย : " + data.homeAddress.alley + "&emsp;ถนน : " + data.homeAddress.road + "&emsp;ตำบล : " + data.homeAddress.district))
                .append($("<div>").html("อำเภอ : " + data.homeAddress.prefecture + "&emsp;จ ังหวัด : " + data.homeAddress.county + "&emsp;รหัสไปรษณีย์ : " + data.homeAddress.post))
            )
            .append($("<div>").addClass("col-sm-2").attr("style","text-align: center;")
                .html(total + " บาท")
            )
        )
        .append($("<hr>").append($("<jsp:text/>")))
}
// Function : SaveBill()
// Manage : Alert เแจ้งว่าได้ทำการบันทึกใบเสร็จการจองแล้ว
function SaveBill() {
    Swal.fire({
        position: 'center',
        type: 'success',
        title: 'บันทึกใบเสร์จการจองเสร็จสิ้น',
        showConfirmButton: false,
        timer: 1500
    })
}
// Function : btnCancelReserv()
// Manage : สร้างปุ่ม "ยกเลิก" เมื่อทำการกดให้ย้อนไปหน้ารายละเอียดบ้านที่ต้องการจะเช่า
function btnCancelReserv() {
    $('div[id="btn"]')
        .append($("<a>").attr("href", "reservation?id=" + id_home)
            .append($("<button>").addClass("btn btn-default").attr("type", "button").attr("title", "ออกจากหน้านี้").html("ยกเลิก"))
        )
        .append("&ensp;")
}