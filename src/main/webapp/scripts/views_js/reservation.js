 // @author  Natthakit  Poltirach
 // @Create Date  2019-08-22
 // @Update Date  2019-08-28
 // @Update Date  2019-08-29
 // @Update Date  2019-08-30
 // @Update Date  2019-09-02
var full_url = document.URL; // Get current url
var url_array = full_url.split('=') // Split the string into an array with / as separator
var id_home = url_array[url_array.length - 1];  // Get the last part of the array (-1)
var data;
$(document).ready(function(){
    //get ข้อมูลบ้านเช่า
    data = $.ajax({
        url: "http://localhost:8080/web/reservDetailHome?id=" + id_home,
        headers: {
            Accept: "application/json",
        },
        type: "GET",
        async: false,
    }).responseJSON;
    GalleryHome();
    reservDetailHome();
    profileRenter();
});
function reservDetailHome() {
   // console.log(data);
    $('div[id="data-home"]')
        .append($("<div>").addClass("row").attr("style", "padding-left:15px")
            .append($("<div>").addClass("container")
                .append($("<div>").addClass("row")
                    .append($("<div>").text("ชื่อบ้าน : " + data.homeAddress.nameHome)
                        .append($("<div>").html("เลขที่บ้าน : " + data.homeAddress.numberHome + "&emsp;หมู่ที่ : " + data.homeAddress.villageNo + "&emsp;หมู่บ้าน : " + data.homeAddress.village + "&emsp;ตรอก/ซอย : " + data.homeAddress.alley + "&emsp;ถนน : " + data.homeAddress.road + "&emsp;ตำบล : " + data.homeAddress.district))
                        .append($("<div>").html("อำเภอ : " + data.homeAddress.prefecture + "&emsp;จังหวัด : " + data.homeAddress.county + "&emsp;รหัสไปรษณีย์ : " + data.homeAddress.post + "&emsp;ขนาดบ้าน : " + data.house_size + " ตารางวา&emsp;ขนาดห้อง : " + data.room_size + " ตารางวา"))
                        .append($("<div>").html("จำนวนห้องน้ำ : " + data.number_toilet + " ห้อง&emsp;จำนวนชั้น : " + data.number_floors + " ชั้น&emsp;จำนวนห้อง : " + data.number_room + " ห้อง"))
                        .append($("<div>").html("ค่าน้ำ/หน่วย : " + data.cost_water + " บาท&emsp;ค่าน้ำ/หน่วย : " + data.cost_fire + " บาท&emsp;ค่าเช่า/เดือน : " + data.cost_hire + " บาท"))
                    )
                    .append($("<a>").attr("href","payment?id=" + data.id_home)
                        .append($("<button>").attr("id","payment").attr("type","button").addClass("btn btn-primary pull-right").attr("data-toggle","modal tooltip").attr("data-target","#payment").attr("title","จองบ้านเช่า").attr("style","position: absolute; right: 25px; bottom: 30px;")
                            .html("จอง")
                        )
                    )
                )
            )
        )
}
// Function : profileRenter()
// Manage : แสดงรายละเอียดเจ้าของบ้านเช่า
function profileRenter() {
    $('span[id="name"]').html(": " + data.user.fullName)
    $('span[id="facebook"]').html(": " + data.user.line_id)
    $('span[id="gmail"]').html(": " + data.user.email)
    $('span[id="local"]').html(": " + data.user.user_address.number_home + "&emsp;หมู่ : "+ data.user.user_address.village_no + "&emsp;ตำบล : " + data.user.user_address.district)
    $('span[id="local2"]').html("&emsp;อำเภอ : " + data.user.user_address.prefecture+ "&emsp;จังหวัด : " + data.user.user_address.county )
    $('span[id="phone"]').html(": " + data.user.phone)
}
// Function : Books()
// Manage : ส่งค่าไป Backend เมื่อกดจอง
function Books() {
    var date = Date.now();
    id_book = "";
    date_book = date;
    bill_book = "จ่ายเงิน";
    status_book = 1;
    id_book_user = "";
    id_book_home = "";
    id_book_contract = "";
    var Books = {
        "id_book":""+id_book,
        "date_book":date_book,
        "bill_book":bill_book,
        "status_book":status_book,
        "id_book_user":id_book_user,
        "id_book_home":id_book_home,
        "id_book_contract": id_book_contract,
    }
   // console.log("Data Books : "+Books);
    $.ajax({
        url: "http://localhost:8080/web/books",
        headers:{
            Accept:"application/json",
        },
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        type:"POST",
        async:false,
        data: JSON.stringify(Books),
        complete:function (xhr) {
        }
    })
}
// Function : GalleryHome()
// Manage : Get ข้อมูลรูปภาพบ้านลงใน Modal
function GalleryHome() {
    gallery = $.ajax({
        url: "http://localhost:8080/web/galleryHome",
        headers: {
            Accept: "application/json",
        },
        type: "GET",
        async: false,
    }).responseJSON;
   // console.log(gallery)
    var size = 0;
    var j = 1;
    for (var i = 0; i < gallery.length; i++) {
        if(gallery[i].home.id_home == id_home){
            size = size+1;
        }
    }
   // console.log("size img : "+size);
    if(size == 1) {
        $('div[id="galleryHome"]')
            .append($("<div>").addClass("col-sm-12")
                .append($("<div>").addClass("row").attr("id","img1")
                )
            )
    }
    else if(size == 2) {
        $('div[id="galleryHome"]')
            .append($("<div>").addClass("col-sm-6")
                .append($("<div>").addClass("row").attr("id","img1")
                )
                .append($("<jsp:text/>"))
            )
            .append($("<div>").addClass("col-sm-6")
                .append($("<div>").addClass("row").attr("id","img2")
                )
            )
    }
    else if(size == 3) {
        $('div[id="galleryHome"]')
            .append($("<div>").addClass("col-sm-8")
                .append($("<div>").addClass("row").attr("id","img1")
                )
            )
            .append($("<div>").addClass("col-sm-4")
                .append($("<div>").addClass("row").attr("id","img2")
                )
                .append($("<div>").addClass("row")
                    .append($("<div>").addClass("col-sm-12")
                        .append($("<div>").addClass("row").attr("id","img3")
                        )
                    )
                )
            )
    }
    else if(size == 4) {
        $('div[id="galleryHome"]')
            .append($("<div>").addClass("col-sm-8")
                .append($("<div>").addClass("row").attr("id","img1")
                )
            )
            .append($("<div>").addClass("col-sm-4")
                .append($("<div>").addClass("row").attr("id","img2")
                )
                .append($("<div>").addClass("row")
                    .append($("<div>").addClass("col-sm-6")
                        .append($("<div>").addClass("row").attr("id","img3")
                        )
                    )
                    .append($("<div>").addClass("col-sm-6")
                        .append($("<div>").addClass("row").attr("id","img4")
                        )
                    )
                )
            )
    }
    else if(size > 4) {
        $('div[id="galleryHome"]')
            .append($("<div>").addClass("col-sm-8")
                .append($("<div>").addClass("row").attr("id","img1")
                )
            )
            .append($("<div>").addClass("col-sm-4")
                .append($("<div>").addClass("row").attr("id","img2")
                )
                .append($("<div>").addClass("row")
                    .append($("<div>").addClass("col-sm-6")
                        .append($("<div>").addClass("row").attr("id","img3")
                        )
                    )
                    .append($("<div>").addClass("col-sm-6")
                        .append($("<div>").addClass("row").attr("id","img4")
                        )
                        .append($("<div>").addClass("more_pic_content").attr("style","display: table; height: 100%; width: 100%;")
                            .append($("<div>").addClass("more_pic_text").attr("data-toggle","modal").attr("data-target","#gallery").attr("style","display: table-cell; text-align: center; vertical-align: middle;")
                                .append($("<font>").css("font-size", 15).html("รูปภาพทั้งหมด"))
                            )
                        )
                    )
                )
            )

    }
    //Loop style size image platform
    for (var i = 0; i < gallery.length; i++) {
        if(gallery[i].home.id_home == id_home){
            $("#img"+j)
                .append($("<img>").addClass("gallery").attr("src", "http://localhost:8080/web/resources/images/imagesHome/" + gallery[i].img_home + ".png").attr("id","size"+j))

            //Condition push image in Modal
            if(size > 4){
                if(j == 1){
                    $('div[id="inner"]')
                        .append($("<div>").addClass("item active")
                            .append($("<img>").attr("src", "http://localhost:8080/web/resources/images/imagesHome/" + gallery[i].img_home + ".png").attr("style","display: table; height: 340px; width: 778px;"))
                        )
                    $('div[id="inner2"]')
                        .append($("<div>").addClass("item active").attr("id","inner2_1")
                            .append($("<div>").addClass("thumb").attr("data-slide-to",j-1).attr("data-target","#carousel")
                                .append($("<img>").addClass("thumb").attr("src", "http://localhost:8080/web/resources/images/imagesHome/" + gallery[i].img_home + ".png").attr("style","display: table; height: 72px; width: 90px;"))
                            )
                        )
                }else{
                    $('div[id="inner"]')
                        .append($("<div>").addClass("item")
                            .append($("<img>").addClass("item").attr("src", "http://localhost:8080/web/resources/images/imagesHome/" + gallery[i].img_home + ".png").attr("style","display: table; height: 340px; width: 778px;"))
                        )
                    $("#inner2_1")
                        .append($("<div>").addClass("thumb").attr("data-slide-to",j-1).attr("data-target","#carousel")
                            .append($("<img>").addClass("thumb").attr("src", "http://localhost:8080/web/resources/images/imagesHome/" + gallery[i].img_home + ".png").attr("style","display: table; height: 72px; width: 90px;"))
                        )
                }

            }
            j++;
        }
    }
    if(size == 1){
        $('img[id="size1"]')
            .attr("style", "width:100%; height:350px;")
    }
    else if(size == 2){
        $('img[id="size1"]')
            .attr("style", "width:100%; height:350px;")
        $('img[id="size2"]')
            .attr("style", "width:100%; height:350px;")
    }
    else if(size == 3){
        $('img[id="size1"]')
            .attr("style", "width:100%; height:350px;")
        $('img[id="size2"]')
            .attr("style", "width:100%; height:175px;")
        $('img[id="size3"]')
            .attr("style", "width:100%; height:175px;")
    }
    else if(size == 4){
        $('img[id="size1"]')
            .attr("style", "width:100%; height:350px;")
        $('img[id="size2"]')
            .attr("style", "width:100%; height:175px;")
        $('img[id="size3"]')
            .attr("style", "width:100%; height:175px;")
        $('img[id="size4"]')
            .attr("style", "width:100%; height:175px;")
    }
    else if(size > 4){
        $('img[id="size1"]')
            .attr("style", "width:100%; height:350px;")
        $('img[id="size2"]')
            .attr("style", "width:100%; height:175px;")
        $('img[id="size3"]')
            .attr("style", "width:100%; height:175px;")
        $('img[id="size4"]')
            .attr("style", "width:100%; height:175px;")
    }

}