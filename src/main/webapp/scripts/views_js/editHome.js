/*
@author  Sarawut Promsoon
@Create Date 2019-09-13
@Update Date 2019-10-03
* file.js : editHome
* Manage : javaScript form v_edit_home
*/

var id_homeEdit;
var county = [
    "ChiangMai",
    "ChiangRai",
    "Lampang",
    "Lamphun",
    "MaeHongSon",
    "Nan",
    "Phayao",
    "Phrae",
    "Uttaradit",
    "Kalasin ",
    "KhonKaen",
    "Chaiyaphum",
    "NakhonPhanom",
    "NakhonRatchasima",
    "BuengKan",
    "Buriram",
    "MahaSarakham",
    "Mukdahan",
    "Yasothon",
    "RoiEt",
    "Loei",
    "SakonNakhon",
    "Surin",
    "Sisaket",
    "NongKhai",
    "NongBuaLamphu",
    "UdonThani",
    "UbonRatchathani",
    "AmnatCharoen",
    "Bangkok",
    "KamphaengPhet",
    "ChaiNat",
    "NakhonNayok",
    "NakhonPathom",
    "NakhonSawan",
    "Nonthaburi",
    "PathumThani",
    "PhraNakhonSiAyutthaya",
    "Phichit",
    "Phitsanulok",
    "Phetchabun",
    "Lopburi",
    "SamutPrakan",
    "SamutSongkhram",
    "SamutSakhon",
    "SingBuri",
    "Sukhothai",
    "SuphanBuri",
    "Saraburi",
    "AngThong",
    "UthaiThani",
    "Chanthaburi",
    "Chachoengsao",
    "Chonburi",
    "Trat",
    "Prachinburi",
    "Rayong",
    "SaKaeo",
    "Kanchanaburi",
    "Tak",
    "PrachuapKhiriKhan",
    "Phetchaburi",
    "Ratchaburi",
    "Krabi",
    "Chumphon",
    "Trang",
    "NakhonSiThammarat",
    "Narathiwat",
    "Pattani",
    "PhangNga",
    "Phatthalung",
    "Phuket",
    "Ranong",
    "Satun",
    "Songkhla",
    "SuratThani",
    "Yala"
];
var size = county.length;
var i = 0;
var data = "";
var iCheck = 0;
var idBank="";
var idBankArr=[];
var checkImg = 0;
var id_user = 1;
$(document).ready(function() {
    var full_url = document.URL;
    var url_array = full_url.split('=')
    var id_home = url_array[url_array.length-1];

    //=== set data County in drop down ===
    setSeleteCounty();

    //=== get data Home by id  ===
    editDataHome(id_home);

    //=== add input Bank When click button ===
    $("#pud").click(function() {
        addInputBank();
    });
    //============== Checked validate one by one ==================
    $("#edit_home_name").change(function () {
        checkOne1("edit_home_name");
    })
    $("#edit_home_number").change(function () {
        checkOne1("edit_home_number");
    })
    $("#edit_village_no").change(function () {
        checkOne1("edit_village_no");
    })
    $("#edit_village").change(function () {
        checkOne1("edit_village");
    })
    $("#edit_alley").change(function () {
        checkOne1("edit_alley");
    })
    $("#edit_road").change(function () {
        checkOne1("edit_road");
    })
    $("#edit_district").change(function () {
        checkOne1("edit_district");
    })
    $("#edit_district_area").change(function () {
        checkOne1("edit_district_area");
    })
    $("#county").change(function () {
        checkOne2("county");
    })
    $("#edit_zipCode").change(function () {
        checkOne1("edit_zipCode");
        if(isNaN($("#edit_zipCode").val())){
            Swal.fire(
                'ใส่ได้เฉพาะตัวเลข!',
                'กรุณาใส่ข้อมูลให้ถูกต้อง',
                'warning'
            )
            $("#edit_zipCode").addClass("classCheck");
        }
    })
    $("#edit_sizeHome").change(function () {
        checkOne1("edit_sizeHome");
    })
    $("#edit_number_floors").change(function () {
        checkOne1("edit_number_floors");
    })
    $("#edit_sizeRoom").change(function () {
        checkOne1("edit_sizeRoom");
    })
    $("#edit_number_room").change(function () {
        checkOne1("edit_number_room");
    })
    $("#edit_number_toilet").change(function () {
        checkOne1("edit_number_toilet");
    })
    $("#edit_water").change(function () {
        checkOne1("edit_water");
    })
    $("#edit_electricity").change(function () {
        checkOne1("edit_electricity");
    })
    $("#edit_month").change(function () {
        checkOne1("edit_month");
    })
    $("#edit_bank0").change(function () {
        checkOne2("edit_bank0");
    })
    $("#edit_bank_number0").change(function () {
        checkOne1("edit_bank_number0");
        if(isNaN($("#edit_bank_number0").val())){
            Swal.fire(
                'ใส่ได้เฉพาะตัวเลข!',
                'กรุณาใส่ข้อมูลให้ถูกต้อง',
                'warning'
            )
            $("#edit_bank_number0").addClass("classCheck");
        }
    })

//=========================================
    $('#multipleUploadForm').submit(function(event) {
        console.log("55:");
        var formElement = this;
        // You can directly create form data from the form element
        // (Or you could get the files from input element and append them to FormData as we did in vanilla javascript)
        var formData = new FormData(formElement);
        console.log(formElement);
        console.log("=====");
        console.log(formData);
        $.ajax({
            type: "POST",
            enctype: 'multipart/form-data',
            url: "http://localhost:8080/web/uploadMultipleFiles?id=U"+id_user+"H"+$("#edit_home_name").val(),
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                console.log(response);
                // process response
            },
            error: function (error) {
                console.log(error);
                // process error
            }
        });
        event.preventDefault();
        $("#imgInput").hide();
        $("#uploadConnect").text("Upload complete").css("color","green");

    });
    imgHiddenInput();
});

function shadow(x) {
    x.classList.add("shadow");
}
function noShadow(x) {
    x.classList.remove("shadow");
}
function RemoveCreate(id){
    var tr_create = document.getElementById("tr_"+id);
    tr_create.parentNode.removeChild(tr_create);
}
function selectBank(id){
    var selectB = document.getElementById(id);
    var text = document.getElementById("f"+id).innerText;
    document.getElementById("bank"+ibank).innerHTML = ""
}
var ibank = 0;
function setbank(i){
    ibank = i;
}
function setSeleteCounty() {
    for (var c = 0; c < size; c++) {
        if (c == 0) {
            $("#county").append('<option value="0">--เลือก--</option>');
        }
        $("#county").append(
            '<option value="' + county[c] + '">' + county[c] + "</option>"
        );
        $("#county option[value="+0+"]").attr("disabled","ture")

    }
}
function addInputBank() {
    i=i+1;
    $("tbody").append($("<tr>").attr("id","tr_"+i)
        .append($("<td>").addClass("col-sm-1")
            .append($("<div>").addClass("row").text("ธนาคาร:")
                .append($("<span>").append("*"))
            )
        )
        .append($("<td>").addClass("col-sm-3")
            .append($("<div>").addClass("col-sm-10")
                .append($("<select>").addClass("form-control").attr("id","edit_bank"+i)
                    .append($("<option>").attr("value","").attr("disabled","ture").text("-- เลือก --"))
                    .append($("<option>").attr("value","BBL").text("ธ. กรุงเทพ"))
                    .append($("<option>").attr("value","KBANK").text("ธ. กสิกรไทย"))
                    .append($("<option>").attr("value","KTB").text("ธ. กรุงไทย"))
                    .append($("<option>").attr("value","BAY").text("ธ. กรุงศรีอยุธยา"))
                    .append($("<option>").attr("value","SCB").text("ธ. ไทยพาณิชย์"))
                )
            )
        )
        .append($("<td>").addClass("col-sm-1")
            .append($("<div>").addClass("row").text("เลขที่บัญชี:")
                .append($("<span>")
                    .append("*")
                )
            )
        )
        .append($("<td>").addClass("col-sm-2")
            .append($("<input>").addClass("form-control").attr("placeholder", "xxx-xxxxxx-x").attr("maxlength", "10").attr("type", "text").attr("id","edit_bank_number"+i)
            )
        )
        .append($("<td>")
            .append($("<button>").addClass("btn btn-danger dele").attr("type","button").attr("onclick",'RemoveCreate("'+i+'")')
                .append($("<i>").addClass("glyphicon glyphicon-remove")
                )
            )
        )
    );

}

function editDataHome(id) {
    data = $.ajax({
        url: "http://localhost:8080/web/editdatahome?id="+id,
        headers:{
            Accept:"application/json"
        },
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        type:"GET",
        async:false,
        data: JSON.stringify(),
        complete:function (xhr) {

        }
    }).responseJSON;
    console.log(data);
    id_homeEdit = id;
    $("#edit_home_name").val(data.homeAddress.nameHome);
    $("#edit_home_number").val(data.homeAddress.numberHome);
    $("#edit_village_no").val(data.homeAddress.villageNo);
    $("#edit_village").val(data.homeAddress.village);
    $("#edit_alley").val(data.homeAddress.alley);
    $("#edit_road").val(data.homeAddress.road);
    $("#edit_district").val(data.homeAddress.prefecture);
    $("#edit_district_area").val(data.homeAddress.district);
    $("#edit_county").val(data.homeAddress.county);
    $("#edit_zipCode").val(data.homeAddress.post);
    $("#edit_sizeHome").val(data.house_size);
    $("#edit_number_floors").val(data.number_floors);
    $("#edit_sizeRoom").val(data.room_size);
    $("#edit_number_room").val(data.number_room);
    $("#edit_number_toilet").val(data.number_toilet);
    $("#edit_water").val(data.cost_water);
    $("#edit_electricity").val(data.cost_fire);
    $("#edit_month").val(data.cost_hire);
    $("#edit_bank0").val(data.bank_account[0].name_bank);
    $("#edit_bank_number0").val(data.bank_account[0].number_account);
    $("#county").val(data.homeAddress.county);
    idBank = data.bank_account[0].id_bank;
    idBankArr.push(idBank);
    editDataBank();
    editDataImage();
}
function editDataBank() {
    data = $.ajax({
        url: "http://localhost:8080/web/editdatabank",
        headers:{
            Accept:"application/json"
        },
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        type:"GET",
        async:false,
        data: JSON.stringify(),
        complete:function (xhr) {
        }
    }).responseJSON;
    var j = 0;
    for(var i = 0 ;i<data.length;i++){
        var text="";
        if(data[i].home.id_home == id_homeEdit && data[i].id_bank != idBank){
            j = j+1;
            addInputBank();
            console.log(data[i])
            $("#edit_bank"+j).val(data[i].name_bank);
            $("#edit_bank_number"+j).val(data[i].number_account);
            idBankArr.push(data[i].id_bank);
        }
    }
}
function editDataImage() {
    data = $.ajax({
        url: "http://localhost:8080/web/editDataImage",
        headers:{
            Accept:"application/json"
        },
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        type:"GET",
        async:false,
        data: JSON.stringify(),
        complete:function (xhr) {
        }
    }).responseJSON;
    console.log(data);
    // $("#imgInput").empty();
    var j=1;
    // $("#imgInput").append($("<h3>").text("Upload Files Home Image").append($("<br>").append($("<jsp:text>"))));
    for(var i = 0;i<data.length;i++){
        if(data[i].home.id_home == id_homeEdit){
            if(j==1){
                $("#imgInput").append("File img : ")
            }else if(j != 1){
                $("#imgInput").append("  ")
            }
            $("#imgInput").append($("<img>").addClass("gallery").attr("src","http://localhost:8080/web/resources/images/imagesHome/"+data[i].img_home+".png"))
            j=j+1;
        }
    }
    $("#imgInput").append(" ").append($("<a>").attr("id","editImg").attr("onclick","imgInput()").addClass("btn btn-warning ").attr("data-toggle","tooltip").attr("title","ลบข้อมูล").append($("<i>").addClass("glyphicon glyphicon-pencil").append($("<jsp:text>"))))

}
function imgHiddenInput() {
    $("#multipleFileUploadInput").attr("type","hidden");
    $("#submitButton").hide();
    $("#Connect").hide();
    $("#editImg").show();
    $("img").show();
    checkImg = 0;
}
function imgInput() {
    $("#multipleFileUploadInput").attr("type","file");
    $("#submitButton").show();
    $("#Connect").show();
    $("#editImg").hide();
    $("img").hide();
    checkImg = 1;
}
function updateEditHome(){
    var bank = [];
    for(var i = 0; i< document.getElementsByTagName("tr").length ;i++){
        if(i < idBankArr.length){
            bank.push({
                "bank": document.getElementsByTagName("tr")[i].getElementsByTagName("td")[1].getElementsByTagName("div")[0].getElementsByTagName("select")[0].value,
                "number_bank":document.getElementsByTagName("tr")[i].getElementsByTagName("td")[3].getElementsByTagName("input")[0].value,
                "id_bank":idBankArr[i]
            })
        }else {
            bank.push({
                "bank": document.getElementsByTagName("tr")[i].getElementsByTagName("td")[1].getElementsByTagName("div")[0].getElementsByTagName("select")[0].value,
                "number_bank":document.getElementsByTagName("tr")[i].getElementsByTagName("td")[3].getElementsByTagName("input")[0].value,
                "id_bank":"no"
            })
        }
    }
    console.log(bank)
    if(validate()){
            var edit = {
                "id_home":id_homeEdit,
                "home_name":$("#edit_home_name").val(),
                "home_number":$("#edit_home_number").val(),
                "village_no":$("#edit_village_no").val(),
                "village":$("#edit_village").val(),
                "alley":$("#edit_alley").val(),
                "road":$("#edit_road").val(),
                "district":$("#edit_district").val(),
                "district_area":$("#edit_district_area").val(),
                "county":$("#county").val(),
                "zipCode":$("#edit_zipCode").val(),

                "img": "U"+id_user,
                "sizeStart":$("img").length,
                "sizeHome":$("#edit_sizeHome").val(),
                "number_floors":$("#edit_number_floors").val(),
                "sizeRoom":$("#edit_sizeRoom").val(),
                "number_room":$("#edit_number_room").val(),
                "number_toilet":$("#edit_number_toilet").val(),
                "water":$("#edit_water").val(),
                "electricity":$("#edit_electricity").val(),
                "month":$("#edit_month").val(),

                "bank":bank,
                "idBankArr":idBankArr.length,
                "size_bank":bank.length

            }

            $.ajax({
                url: "http://localhost:8080/web/updateEditHome",
                headers:{
                    Accept:"application/json",
                },
                contentType:"application/json; charset=utf-8",
                dataType:"json",
                type:"POST",
                async:false,
                data: JSON.stringify(edit),
                complete:function (xhr) {
                }
            })
            Swal.fire({
                position: 'top-end',
                type: 'success',
                title: 'แก้ไขสำเร็จ',
                showConfirmButton: false,
                timer: 1500
            })
            setTimeout(clickLink,2000)
        }else{
            Swal.fire(
                'ไม่สามารถแก้ไขได้!',
                'กรุณาใส่ข้อมูลให้ครบ',
                'warning'
            )
        }
}
function clickLink() {
    var link = document.createElement("a");
    link.href = "http://localhost:8080/web/own_manage";
    link.click();
}
function validate() {
    var home_name = $("#edit_home_name").val();
    var home_number = $("#edit_home_number").val();
    var village_no = $("#edit_village_no").val();
    var village = $("#edit_village").val();
    var alley = $("#edit_alley").val();
    var road =  $("#edit_road").val();
    var district = $("#edit_district").val();
    var district_area = $("#edit_district_area").val();
    var county = $("#county").val();
    var zipCode = $("#edit_zipCode").val();

    var sizeHome = $("#edit_sizeHome").val();
    var number_floors = $("#edit_number_floors").val();
    var sizeRoom = $("#edit_sizeRoom").val();
    var number_room = $("#edit_number_room").val();
    var number_toilet = $("#edit_number_toilet").val();
    var water = $("#edit_water").val();
    var electricity = $("#edit_electricity").val();
    var month = $("#edit_month").val();

    var bank0 = $("#edit_bank0").val()
    var bank_number0 = $("#edit_bank_number0").val()
    var imgEdit = "";
    var checkBank;

    if(home_name == ""){
        $("#edit_home_name").addClass("classCheck");
    }else{
        $("#edit_home_name").removeClass("classCheck")
    }
    if(home_number == ""){
        $("#edit_home_number").addClass("classCheck");
    }else{
        $("#edit_home_number").removeClass("classCheck")
    }
    if(village_no == ""){
        $("#edit_village_no").addClass("classCheck");
    }else{
        $("#edit_village_no").removeClass("classCheck")
    }
    if(village == ""){
        $("#edit_village").addClass("classCheck");
    }else{
        $("#edit_village").removeClass("classCheck")
    }
    if(alley == ""){
        $("#edit_alley").addClass("classCheck");
    }else{
        $("#edit_alley").removeClass("classCheck")
    }
    if(road == ""){
        $("#edit_road").addClass("classCheck");
    }else{
        $("#edit_road").removeClass("classCheck")
    }
    if(district == ""){
        $("#edit_district").addClass("classCheck");
    }else{
        $("#edit_district").removeClass("classCheck")
    }
    if(district_area == ""){
        $("#edit_district_area").addClass("classCheck");
    }else{
        $("#edit_district_area").removeClass("classCheck")
    }
    if(county == 0){
        $("#county").addClass("classCheck");
    }else{
        $("#county").removeClass("classCheck")
    }
    if(zipCode == ""){
        $("#edit_zipCode").addClass("classCheck");
    }else{
        $("#edit_zipCode").removeClass("classCheck")
    }
    //===========================
    if(sizeHome == ""){
        $("#edit_sizeHome").addClass("classCheck");
    }else{
        $("#edit_sizeHome").removeClass("classCheck")
    }
    if(number_floors == ""){
        $("#edit_number_floors").addClass("classCheck");
    }else{
        $("#edit_number_floors").removeClass("classCheck")
    }
    if(sizeRoom == ""){
        $("#edit_sizeRoom").addClass("classCheck");
    }else{
        $("#edit_sizeRoom").removeClass("classCheck")
    }
    if(number_room == ""){
        $("#edit_number_room").addClass("classCheck");
    }else{
        $("#edit_number_room").removeClass("classCheck")
    }
    if(number_toilet == ""){
        $("#edit_number_toilet").addClass("classCheck");
    }else{
        $("#edit_number_toilet").removeClass("classCheck")
    }
    if(water == ""){
        $("#edit_water").addClass("classCheck");
    }else{
        $("#edit_water").removeClass("classCheck")
    }
    if(electricity == ""){
        $("#edit_electricity").addClass("classCheck");
    }else{
        $("#edit_electricity").removeClass("classCheck")
    }
    if(month == ""){
        $("#edit_month").addClass("classCheck");
    }else{
        $("#edit_month").removeClass("classCheck")
    }

    if(bank0 == 0){
        $("#edit_bank0").addClass("classCheck");
    }else{
        $("#edit_bank0").removeClass("classCheck")
    }
    if(bank_number0 == ""){
        $("#edit_bank_number0").addClass("classCheck");
    }else{
        $("#edit_bank_number0").removeClass("classCheck")
    }
    if(checkImg == 0){
        imgEdit = "ok";
    }else if(checkImg == 1){
        imgEdit = $("#multipleFileUploadInput").val();
        if(imgEdit == ""){
            $("#cancelImage").append("กรุณา upload รูปภาพ").css("color","red");
        }else{
            $("#multipleFileUploadInput").removeClass("classCheck");
        }
    }
    for(var i = 0; i< document.getElementsByTagName("tr").length ;i++){
        var data1 = document.getElementsByTagName("tr")[i].getElementsByTagName("td")[1].getElementsByTagName("div")[0].getElementsByTagName("select")[0].value
        var data2 = document.getElementsByTagName("tr")[i].getElementsByTagName("td")[3].getElementsByTagName("input")[0].value
        if(data1 != "" && data2 !=""){
            checkBank = "1";
        }else{
            checkBank = "";
            break;
        }
    }
    if(home_name != ""&& home_number != "" && village_no != "" && village != "" && alley != "" && road != "" && district != "" && district_area != "" && county != "" && zipCode != "" && sizeHome != "" && number_floors != "" && sizeRoom != "" && number_room != "" && number_toilet != "" && water != "" && electricity != "" && month != "" && imgEdit != "" && checkBank != ""){
        return true;
    }else{
        return false;
    }
}
function checkOne1(name) {
    if($("#"+name).val() == ""){
        $("#"+name).addClass("classCheck");
    }else{
        $("#"+name).removeClass("classCheck")
    }
}
function checkOne2(name) {
    if($("#"+name).val() == 0){
        $("#"+name).addClass("classCheck");
    }else{
        $("#"+name).removeClass("classCheck")
    }
}
