 // @Author Natthakit Poltirach
 // @Create Date  2019-09-27
 // @Update Date  2019-10-10
var full_url = document.URL; // Get current url
var url_array = full_url.split('=') // Split the string into an array with / as separator
var id_home = url_array[url_array.length - 1];  // Get the last part of the array (-1)
$(document).ready(function(){
    //Post ข้อมูล Inform ไปยัง Backend เพื่อทำการเพิ่ม Inform ลง Database
    btnReport();
    $('button[id="btn_report"]').click(function () {
       // console.log(id_home)
        var topic = "";
        var desciption = "";
        var date = Date.now();
        var calendar = new Date(date);
        var day = ("0" + calendar.getDate()).slice(-2);
        var month = ("0" + (calendar.getMonth() + 1)).slice(-2);
        var today = calendar.getFullYear() + "-" + (month) + "-" + (day);
        var status_inform = 1;
        var id_user = 2;
        topic = $('select[id="hReport"]').val();
        desciption = $('textarea[id="bReport"]').val();
        if($('select[id="hReport"]').val() != null && $('textarea[id="bReport"]').val() != ""){
            var Inform = {
                "topic": topic,
                "desciption": desciption,
                "date_submit": today,
                "status_inform": status_inform,
                "id_home": id_home,
                "id_user": id_user
            }
            $.ajax({
                url: "http://localhost:8080/web/probInform",
                headers: {
                    Accept: "application/json",
                },
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                async: false,
                data: JSON.stringify(Inform),
                complete: function (xhr) {
                }
            })
            Swal.fire({
                position: 'center',
                type: 'success',
                title: 'แจ้งปัญหาเสร็จสิ้น',
                showConfirmButton: false,
                timer: 2000
            }).then((result) => {
                if (result.value) {
                    var link = document.createElement("a");
                    link.href = "http://localhost:8080/web/home_detail?id="+ id_home;
                    link.click();
                }
                else{
                    link = document.createElement("a");
                    link.href = "http://localhost:8080/web/home_detail?id="+ id_home;
                    link.click();
                }
            });
        }
        else{
            Swal.fire({
                type: 'info',
                title: 'เกิดข้อผิดพลาด',
                text: 'กรุณากรอกข้อมูลให้ครบถ้วน!',
            })
        }
    })
});
 // Function : btnReport(
 // Manage : ปุ่มการแจ้งปัญหา
function btnReport() {
    $('div[id="btnReport"]')
        .append($("<button>").addClass("btn btn-primary pull-right").attr("id","btn_report").attr("type","button").attr("title","ยืนยันการแจ้งปัญหา")
            .html("ยืนยัน")
        )
}
