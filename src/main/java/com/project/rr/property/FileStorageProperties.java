package com.project.rr.property;

import org.springframework.boot.context.properties.ConfigurationProperties;

/*
@author  Sarawut Promsoon
@Create Date  2019-10-01
@Update Date
* Class : FileStorageProperties
* Manage :
*/
@ConfigurationProperties(prefix = "file")
public class FileStorageProperties {
    private String uploadDir;

    public String getUploadDir() {
        return uploadDir;
    }

    public void setUploadDir(String uploadDir) {
        this.uploadDir = uploadDir;
    }
}
