package com.project.rr.payload;

/*
@author  Sarawut Promsoon
@Create Date  2019-10-01
@Update Date
* Class : UploadFileResponse
* Manage :
*/
public class UploadFileResponse {
    private String fileName;
    private String fileDownloadUri;
    private String fileType;
    private long size;

    public UploadFileResponse(String fileName, String fileDownloadUri, String fileType, long size) {
        this.fileName = fileName;
        this.fileDownloadUri = fileDownloadUri;
        this.fileType = fileType;
        this.size = size;
    }
    // Getters and Setters (Omitted for brevity)
}
