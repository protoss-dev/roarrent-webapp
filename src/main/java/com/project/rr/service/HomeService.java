package com.project.rr.service;

import org.springframework.http.ResponseEntity;

/*
@author  Sarawut Promsoon
@Create Date  2019-09-11
@Update Date  2019-10-01
* interface : HomeService
* Manage :
*/
public interface HomeService {
    ResponseEntity<String> getHome();
    ResponseEntity<String> getNotify();
    ResponseEntity<String> getContract();
    ResponseEntity<String> getDataUser();
    ResponseEntity<String> getDataBank();
    ResponseEntity<String> getDataImage();


}
