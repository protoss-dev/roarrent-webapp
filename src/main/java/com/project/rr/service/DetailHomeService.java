package com.project.rr.service;

import org.springframework.http.ResponseEntity;

/*
@author  Natthakit Poltirach
@Create Date  2019-09-16
@Update Date  2019-09-18
* Interface : DetailHomeService
* Manage : get data through DetailHomeService
*/
public interface DetailHomeService {

    ResponseEntity<String> getDetailHome();

    ResponseEntity<String> getReservDetailHome(String id_home);

    ResponseEntity<String> getListBooks();

    ResponseEntity<String> getHomeBooks(String id_books);

    ResponseEntity<String> getInform();

    ResponseEntity<String> getGalleryHome();
}
