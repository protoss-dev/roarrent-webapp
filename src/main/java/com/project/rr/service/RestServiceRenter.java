package com.project.rr.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

/*
@author  Natthakit Poltirach
@Create Date  2019-09-11
@Update Date  2019-09-12
@Update Date  2019-09-16
* class : RestServiceRenter
* Is Service
*/
@Service
public class RestServiceRenter implements DetailHomeService {
    static Logger LOGGER = LoggerFactory.getLogger(RestServiceRenter.class);

    /*
    @author Natthakit Poltirach
    @Create Date  2019-09-16
    @Update Date  -
    * method : editProfile
    * Manage : Get ข้อมูลจาก Backend
    */
    public ResponseEntity<String> editProfile(String id_user){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<String>("",headers);
        String url = "http://localhost:8081/App/editProfile?id="+id_user;
        try{
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET,entity,String.class);
            return responseEntity;
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Error : " + e.getMessage());
            return null;
        }
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-09-16
    @Update Date  2019-09-17
    * method : getDetailHome()
    * Manage : Get ข้อมูลจาก Backend
    */
    @Override
    public ResponseEntity<String> getDetailHome() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "http://localhost:8081/App/getDetailHome";
        try {
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
            return responseEntity;
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-09-16
    @Update Date  2019-09-17
    * method : updateEditProfile(String json)
    * Manage : Post ข้อมูลไปยัง Backend
    */
    public ResponseEntity<String> updateEditProfile(String json){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>(json,headers);
        String url = "http://localhost:8081/App/updateProfile";
        try{
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST,entity,String.class);
            return responseEntity;
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Error : " + e.getMessage());
            return null;
        }
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-09-16
    @Update Date  2019-09-17
    * method : getReservDetailHome(String id_home)
    * Manage : Get ข้อมูลจาก Backend
    */
    @Override
    public ResponseEntity<String> getReservDetailHome(String id_home){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>("",headers);
        String url = "http://localhost:8081/App/getReservDetailHome?id="+id_home;
        try{
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET,entity,String.class);
            return responseEntity;

        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Error : " + e.getMessage());
            return null;
        }
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-09-16
    @Update Date  2019-09-17
    * method : getListBooks()
    * Manage : Get ข้อมูลจาก Backend
    */
    @Override
    public ResponseEntity<String> getListBooks() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "http://localhost:8081/App/getListBooks";
        try {
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
            return responseEntity;
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-09-25
    @Update Date  -
    * method : addBooks(String json)
    * Manage : Post ข้อมูลไปยัง Backend
    */
    public ResponseEntity<String> addBooks(String json){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>(json,headers);
        String url = "http://localhost:8081/App/addBooks";
        try{
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST,entity,String.class);
            return responseEntity;
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Error : " + e.getMessage());
            return null;
        }
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-09-25
    @Update Date  -
    * method : probInform(String json)
    * Manage : Post ข้อมูลไปยัง Backend
    */
    public ResponseEntity<String> probInform(String json){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>(json,headers);
        String url = "http://localhost:8081/App/probInform";
        try{
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST,entity,String.class);
            return responseEntity;
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Error : " + e.getMessage());
            return null;
        }
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-09-30
    @Update Date  -
    * method : getHomeBooks(String id_books)
    * Manage : Get ข้อมูลจาก Backend
    */
    @Override
    public ResponseEntity<String> getHomeBooks(String id_books){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>("",headers);
        String url = "http://localhost:8081/App/getHomeBooks?id="+id_books;
        try{
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET,entity,String.class);
            return responseEntity;

        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Error : " + e.getMessage());
            return null;
        }
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-10-1
    @Update Date  -
    * method : updateHomeBooks(String json)
    * Manage : Post ข้อมูลไปยัง Backend
    */
    public ResponseEntity<String> updateHomeBooks(String json){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>(json,headers);
        String url = "http://localhost:8081/App/updateHomeBooks";
        try{
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST,entity,String.class);
            return responseEntity;
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Error : " + e.getMessage());
            return null;
        }
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-10-1
    @Update Date  -
    * method : getInform()
    * Manage : Get ข้อมูลจาก Backend
    */
    @Override
    public ResponseEntity<String> getInform() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type", "application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "http://localhost:8081/App/getInform";
        try {
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
            return responseEntity;
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-10-1
    @Update Date  -
    * method : updateHomeRent(String json)
    * Manage : Post ข้อมูลไปยัง Backend
    */
    public ResponseEntity<String> updateHomeRent(String json){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>(json,headers);
        String url = "http://localhost:8081/App/updateHomeRent";
        try{
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST,entity,String.class);
            return responseEntity;
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Error : " + e.getMessage());
            return null;
        }
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-10-7
    @Update Date  -
    * method : getGalleryHome
    * Manage : Get ข้อมูลจาก Backend
    */
    @Override
    public ResponseEntity<String> getGalleryHome() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type", "application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "http://localhost:8081/App/getGalleryHome";
        try {
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
            return responseEntity;
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}

