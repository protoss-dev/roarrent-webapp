package com.project.rr.service;

import com.fasterxml.jackson.databind.util.JSONPObject;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

/*
@author  Sarawut Promsoon
@Create Date  2019-09-11
@Update Date  2019-10-02
* class : RestService
* Is Service
*/
@Service
public class RestServiceOwn implements HomeService{
    static Logger LOGGER = LoggerFactory.getLogger(RestServiceOwn.class);

    /*
    * method : addHome
    * Manage : data send to bankEnd
    */
    public ResponseEntity<String> addHome(String json,int i){
        System.out.println("service = "+i);
        String jsonNew = json.subSequence(0,json.lastIndexOf("}"))+","+'"'+"sizeImg"+'"'+":"+i+"}";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>(jsonNew,headers);
        String url = "http://localhost:8081/App/add";
        try{
//            System.out.println("service_url"+json);
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST,entity,String.class);
            return responseEntity;
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Error : " + e.getMessage());
            return null;
        }
    }

    /*
    * method : getHome
    * Manage : get data through bankEnd
    */
    @Override
    public ResponseEntity<String> getHome() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "http://localhost:8081/App/getHome";
        try {
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);
            return responseEntity;
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }


    /*
   * method : getEditHome
   * Manage : get data through bankEnd
   */
    public ResponseEntity<String> getEditHome(String id_home) {
        String url = "http://localhost:8081/App/getEditHome?id="+id_home;
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>("", headers);

        try {
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET,entity,String.class);
            return responseEntity;
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
    /*
    * method : updateEditHome
    * Manage : data send to bankEnd
    */
    public ResponseEntity<String> updateEditHome(String json,int i){
        System.out.println("service = "+i);
        String jsonNew = json.subSequence(0,json.lastIndexOf("}"))+","+'"'+"sizeImg"+'"'+":"+i+"}";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>(jsonNew,headers);
        String url = "http://localhost:8081/App/updateHome";
        try{
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST,entity,String.class);
//            System.out.println("service_url"+json);
            return responseEntity;
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Error : " + e.getMessage());
            return null;
        }
    }

    /*
    * method : getAddressHome
    * Manage : get data through bankEnd
    */
    @Override
    public ResponseEntity<String> getNotify() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "http://localhost:8081/App/getNotify";
        try {

            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity, String.class);

            return responseEntity;
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /*
    * method : updateStatusInform
    * Manage : data send to bankEnd
    */
    public ResponseEntity<String> updateStatusInform(String json){

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>(json,headers);
        String url = "http://localhost:8081/App/updateStatusNotify";
        try{
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST,entity,String.class);

            return responseEntity;
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Error : " + e.getMessage());
            return null;
        }
    }

    /*
   * method : getDataBook
   * Manage : get data through bankEnd
   */
    public ResponseEntity<String> getDataBook() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "http://localhost:8081/App/getDataBook";
        try {
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET,entity,String.class);
            return responseEntity;
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
    /*
   * method : deleteHome
   * Manage : update status home
   */
    public ResponseEntity<String> deleteHome(String json) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>(json,headers);
        String url = "http://localhost:8081/App/deleteHome";
        try{
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST,entity,String.class);
            return responseEntity;
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Error : " + e.getMessage());
            return null;
        }
    }

    /*
    * method : saveContract
    * Manage : Save data Contract
    */
    public ResponseEntity<String> saveContract(String json){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>(json,headers);
        String url = "http://localhost:8081/App/saveContract";
        try{
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST,entity,String.class);
            return responseEntity;
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Error : " + e.getMessage());
            return null;
        }
    }

    /*
   * method : pdfContract
   * Manage : Save Contract pdf
   */
    public ResponseEntity<String> pdfContract(String json){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>(json,headers);
        String url = "http://localhost:8081/App/pdfContract";
        try{
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST,entity,String.class);
            return responseEntity;
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Error : " + e.getMessage());
            return null;
        }
    }

    /*
    * method : deleteContract
    * Manage : update status Contract
    */
    public ResponseEntity<String> deleteContract(String json){
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>(json,headers);
        String url = "http://localhost:8081/App/deleteContract";
        try{
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST,entity,String.class);
            return responseEntity;
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Error : " + e.getMessage());
            return null;
        }
    }

    /*
    * method : getContract
    * Manage : get data through bankEnd
    */
    @Autowired
    public ResponseEntity<String> getContract() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "http://localhost:8081/App/getContract";
        try {
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET,entity,String.class);
            return responseEntity;
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /*
    * method : getDataUser
    * Manage : get data user
    */
    @Autowired
    public ResponseEntity<String> getDataUser() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "http://localhost:8081/App/getDataUser";
        try {
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET,entity,String.class);
            return responseEntity;
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /*
    * method : getDataBank
    * Manage : get data bank for home
    */
    @Autowired
    public ResponseEntity<String> getDataBank() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "http://localhost:8081/App/getDataBank";
        try {
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET,entity,String.class);
            return responseEntity;
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /*
    * method : getDataImage
    * Manage : det data Image for home
    */
    @Autowired
    public ResponseEntity<String> getDataImage() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.add("Contect-Type","application/json; charset=utf-8");
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        String url = "http://localhost:8081/App/getDataImage";
        try {
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET,entity,String.class);
            return responseEntity;
        } catch (Exception e) {
            LOGGER.error("{}", e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}

