package com.project.rr;

import com.project.rr.property.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({
        FileStorageProperties.class
})
public class RrApplication {

    public static void main(String[] args) {
        SpringApplication.run(RrApplication.class, args);
    }

}
