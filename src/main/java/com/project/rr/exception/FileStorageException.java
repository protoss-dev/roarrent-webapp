package com.project.rr.exception;

/*
@author  Sarawut Promsoon
@Create Date  2019-10-01
@Update Date
* Class : FileStorageException
* Manage :
*/
public class FileStorageException extends RuntimeException {
    public FileStorageException(String message) {
        super(message);
    }
    public FileStorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
