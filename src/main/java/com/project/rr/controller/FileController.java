package com.project.rr.controller;


import com.project.rr.payload.UploadFileResponse;
import com.project.rr.service.FileStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/*
@author  Sarawut Promsoon
@Create Date  2019-10-01
@Update Date
* Class : FileController
* Manage : uploadFile
*/
@Controller
@RestController
public class FileController {
    private static final Logger logger = LoggerFactory.getLogger(FileController.class);
    public static int sizeImg = 0;
    public static int number = 1;
    @Autowired
    private FileStorageService fileStorageService;

    /*
    * method : uploadFile
    * Manage : uploadFile 1 file
    */
    @PostMapping("/uploadFile")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file ,String id_home,int sizeImg) {
        System.out.println(id_home);
        String fileName = fileStorageService.storeFile(file,id_home,sizeImg);
        System.out.println(fileName);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/downloadFile/")
                .path(fileName)
                .toUriString();
        System.out.println(fileDownloadUri);
        return new UploadFileResponse(fileName, fileDownloadUri,
                file.getContentType(), file.getSize());
    }

    /*
    * method : uploadMultipleFiles
    * Manage : uploadFile Multiple
    */
    @PostMapping("/uploadMultipleFiles")
    public List<UploadFileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files,@RequestParam(name = "id") String id) {
        System.out.println(files.length);
        sizeImg = files.length;
        System.out.println("-------------");
         Arrays.asList(files)
                .stream()
                .map(file -> uploadFile(file,id,number++))
                .collect(Collectors.toList());
        number = 1;
        return null;
    }

    /*
    * method : downloadFile
    * Manage : -
    */
    @GetMapping("/downloadFile/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
        // Load file as Resource
        Resource resource = fileStorageService.loadFileAsResource(fileName);
        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            logger.info("Could not determine file type.");
        }
        // Fallback to the default content type if type could not be determined
        if(contentType == null) {
            contentType = "application/octet-stream";
        }
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }
}
