package com.project.rr.controller;

import com.project.rr.service.DetailHomeService;

import com.project.rr.service.RestServiceRenter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/*
@author  Natthakit Poltirach
@Create Date  2019-09-09
@Update Date  2019-09-12
@Update Date  2019-09-16
* class : Renter_management
* Manage : Renter Management
*/
@Controller
public class Renter_management {
    @Autowired
    RestServiceRenter restServiceRenter;

    @Autowired
    DetailHomeService detailHomeService;


    @GetMapping("/renter_manage")
    public String renter_manage() {
        return "v_renter_main";
    }

    @GetMapping("/profile_manage")
    public String profile_manage() {
        return "v_profile";
    }

    @GetMapping("/home_detail")
    public String home_detail() {
        return "v_detail";
    }

    @GetMapping("/reservation")
    public String reservation() {
        return "v_reservation";
    }

    @GetMapping("/payment")
    public String payment() {
        return "v_payment";
    }

    @GetMapping("/ProbInform")
    public String ProbInform() {
        return "v_prob_inform";
    }

    @GetMapping("/ReservCancel")
    public String ReservCancel() {
        return "v_reserv_cancel";
    }

    @GetMapping("/RentCancel")
    public String RentCancel() {
        return "v_rent_cancel";
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-09-11
    @Update Date  2019-09-12
    * method : editProfile()
    * Manage : ส่งข้อมูลของ User จาก JS ไปยัง Service
    */
    @RequestMapping(value = "/profile", method = RequestMethod.GET, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> editProfile(@RequestParam(name = "id") String id_user) {//String id_user คือ ค่าของ id_user
        ResponseEntity<String> responseEntity = restServiceRenter.editProfile(id_user);
        return responseEntity;
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-09-10
    @Update Date  2019-09-11
    * method : showDetail()
    * Manage : ส่งข้อมูลของบ้านจาก JS ไปยัง Service
    */
    @RequestMapping(value = "/showDetailHome", method = RequestMethod.GET, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> showDetail() {
        ResponseEntity<String> responseEntity = detailHomeService.getDetailHome();
        return responseEntity;
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-09-17
    @Update Date  -
    * method : updateProfile
    * Manage : ส่งข้อมูลของ User จาก JS ไปยัง Service
    */
    @RequestMapping(value = "/updateProfile", method = RequestMethod.POST, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> updateProfile(@RequestBody String json) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;sharset=utf-8");
        try {
            return restServiceRenter.updateEditProfile(json);
        } catch (Exception e) {
            return new ResponseEntity<String>("200", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-09-18
    @Update Date  -
    * method : reservDetailHome()
    * Manage : ส่งข้อมูลของบ้าน จาก JS ไปยัง Service
    */
    @RequestMapping(value = "/reservDetailHome", method = RequestMethod.GET, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> reservDetailHome(@RequestParam(name = "id") String id_home) {
        ResponseEntity<String> responseEntity = detailHomeService.getReservDetailHome(id_home);
        return responseEntity;
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-09-23
    @Update Date  -
    * method : listBooks()
    * Manage : ส่งข้อมูลของการจอง จาก JS ไปยัง Service
    */
    @RequestMapping(value = "/listBooks", method = RequestMethod.GET, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> listBooks() {
        ResponseEntity<String> responseEntity = detailHomeService.getListBooks();
        return responseEntity;
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-09-25
    @Update Date  -
    * method : homeDetail()
    * Manage : ส่งข้อมูลของบ้าน จาก JS ไปยัง Service
    */
    @RequestMapping(value = "/homeDetail", method = RequestMethod.GET, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> homeDetail(@RequestParam(name = "id") String id_home) {
        ResponseEntity<String> responseEntity = detailHomeService.getReservDetailHome(id_home);
        return responseEntity;
    }
        /*
    @author  Natthakit Poltirach
    @Create Date  2019-09-25
    @Update Date  -
    * method : Books
    * Manage : ส่งข้อมูลของการจอง จาก JS ไปยัง Service
    */
    @RequestMapping(value = "/books", method = RequestMethod.POST, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> Books(@RequestBody String json) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;sharset=utf-8");
        try {
            return restServiceRenter.addBooks(json);
        } catch (Exception e) {
            return new ResponseEntity<String>("200", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-09-27
    @Update Date  -
    * method : probInform()
    * Manage : ส่งข้อมูลการแจ้ง จาก JS ไปยัง Service
    */
    @RequestMapping(value = "/probInform", method = RequestMethod.POST, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> probInform(@RequestBody String json) {
        //System.out.println("Contoller probInform :"+json);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;sharset=utf-8");
        try {
            return restServiceRenter.probInform(json);
        } catch (Exception e) {
            return new ResponseEntity<String>("200", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-09-17
    @Update Date  -
    * method : updateHomeBooks()
    * Manage : ส่งข้อมูลของการจอง จาก JS ไปยัง Service
    */
    @RequestMapping(value = "/updateHomeBooks", method = RequestMethod.POST, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> updateHomeBooks(@RequestBody String json) {
        //      System.out.println("updateProfile :"+json);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;sharset=utf-8");
        try {
            return restServiceRenter.updateHomeBooks(json);
        } catch (Exception e) {
            return new ResponseEntity<String>("200", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-09-30
    @Update Date  -
    * method : getHomeBooks()
    * Manage : ส่งข้อมูลของการจอง จาก JS ไปยัง Service
    */
    @RequestMapping(value = "/homeBooks", method = RequestMethod.GET, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> getHomeBooks(@RequestParam(name = "id") String id_books) {
//        System.out.println("Controller : " + id_books);
        ResponseEntity<String> responseEntity = detailHomeService.getHomeBooks(id_books);
        return responseEntity;
        //      return  null;
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-10-1
    @Update Date  -
    * method : getInform()
    * Manage : ส่งข้อมูลการแจ้ง จาก JS ไปยัง Service
    */
    @RequestMapping(value = "/getInform", method = RequestMethod.GET, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> getInform() {
        ResponseEntity<String> responseEntity = detailHomeService.getInform();
        return responseEntity;
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-10-03
    @Update Date  -
    * method : updateHomeRent()
    * Manage : ส่งข้อมูลของการจอง จาก JS ไปยัง Service
    */
    @RequestMapping(value = "/updateHomeRent", method = RequestMethod.POST, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> updateHomeRent(@RequestBody String json) {
        //      System.out.println("updateProfile :"+json);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json;sharset=utf-8");
        try {
            return restServiceRenter.updateHomeRent(json);
        } catch (Exception e) {
            return new ResponseEntity<String>("200", headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /*
    @author  Natthakit Poltirach
    @Create Date  2019-10-7
    @Update Date  -
    * method : galleryHome()
    * Manage : ส่งข้อมูลของบ้าน จาก JS ไปยัง Service
    */
    @RequestMapping(value = "/galleryHome", method = RequestMethod.GET, produces = "text/html;charset=utf-8", headers = "Accept=application/json")
    public ResponseEntity<String> galleryHome() {
        ResponseEntity<String> responseEntity = detailHomeService.getGalleryHome();
        return responseEntity;
    }
}
