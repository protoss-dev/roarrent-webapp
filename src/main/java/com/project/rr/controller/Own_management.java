package com.project.rr.controller;

import com.project.rr.ThaiBaht;
import com.project.rr.service.HomeService;
import com.project.rr.service.RestServiceOwn;
import net.sf.jasperreports.engine.*;
import org.apache.pdfbox.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/*
@author  Sarawut Promsoon
@Create Date  2019-09-09
@Update Date  2019-09-12
* Class : Own_management
* Manage : manage home
*/
@Controller
public class Own_management {

    @Autowired
    RestServiceOwn restServiceOwn;

    @Autowired
    HomeService homeService;

    public int sizeImg;

    public void setSizeImg(int sizeImg) {
        this.sizeImg = sizeImg;
    }

    //Show screen v_menu
    @GetMapping("/menu")
    public String index(){
        return "v_menu" ;
    }

    //Show screen v_manage_own
    @GetMapping("/own_manage")
    public String own_manage(){
        return "v_manage_own" ;
    }

    //Show screen v_addHome
    @GetMapping("/add_home")
    public String add_home(){ return "v_addHome" ; }

    //Show screen v_contract
    @GetMapping("/contract")
    public String contract(){ return "v_contract" ; }

    //Show screen v_notify
    @GetMapping("/notify")
    public String own_notify(){ return "v_notify" ; }

    //Show screen v_editHome
    @GetMapping("/edit_home")
    public String edit_home(){
        return "v_editHome" ;
    }

    /*
    * method : addHome
    * Manage : data send to restService
    */
    @RequestMapping(value = "/datahome" , method = RequestMethod.POST,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public ResponseEntity<String> addHome(@RequestBody String json ){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type","application/json; charset=utf-8");
        int i = FileController.sizeImg;
        System.out.println(json);
        try{
            return restServiceOwn.addHome(json,i);
        }catch (Exception e){
            return  new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
    * method : showdatahome
    * Manage : get data through homeService
    */
    @RequestMapping(value = "/showHome" , method = RequestMethod.GET,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public ResponseEntity<String> showdatahome(){
        ResponseEntity<String> responseEntity = homeService.getHome();
        return responseEntity;
    }


    /*
    * method : editdatahome
    * Manage : get data through homeService
    */
    @RequestMapping(value = "/editdatahome" , method = RequestMethod.GET,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public ResponseEntity<String> editdatahome(@RequestParam(name = "id") String id_home){
        ResponseEntity<String> responseEntity = restServiceOwn.getEditHome(id_home);
        return responseEntity;
    }

    /*
    * method : updateEditHome
    * Manage : update data home
    */
    @RequestMapping(value = "/updateEditHome" , method = RequestMethod.POST,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public ResponseEntity<String> updateEditHome(@RequestBody String json){
        System.out.println(json);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type","application/json; charset=utf-8");
        int i = FileController.sizeImg;
        try{
            return restServiceOwn.updateEditHome(json,i);
        }catch (Exception e){
            return  new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
    * method : showNotify
    * Manage : get data Notify
    */
    @RequestMapping(value = "/showNotify" , method = RequestMethod.GET,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public ResponseEntity<String> showNotify(){
        ResponseEntity<String> responseEntity = homeService.getNotify();
        return responseEntity;
    }

    /*
   * method : updateStatusInform
   * Manage : data send to restService
   */
    @RequestMapping(value = "/updateStatusInform" , method = RequestMethod.POST,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public ResponseEntity<String> updateStatusInform(@RequestBody String json){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type","application/json; charset=utf-8");
        try{
            return restServiceOwn.updateStatusInform(json);
        }catch (Exception e){
            return  new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
    * method : getDataBook
    * Manage : get data book
    */
    @RequestMapping(value = "/getDataBook" , method = RequestMethod.GET,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public ResponseEntity<String> getDataBook(){
        ResponseEntity<String> responseEntity = restServiceOwn.getDataBook();
        return responseEntity;
    }

    /*
    * method : deleteHome
    * Manage : update status home
    */
    @RequestMapping(value = "/deleteHome" , method = RequestMethod.POST,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public ResponseEntity<String> deleteHome(@RequestBody String json){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type","application/json; charset=utf-8");
        try{
            return restServiceOwn.deleteHome(json);
        }catch (Exception e){
            return  new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
    * method : saveContract
    * Manage : Save data Contract
    */
    @RequestMapping(value = "/saveContract" , method = RequestMethod.POST,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public ResponseEntity<String> saveContract(@RequestBody String json){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type","application/json; charset=utf-8");
        try{
            return restServiceOwn.saveContract(json);
        }catch (Exception e){
            return  new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
    * method : saveContractPDF
    * Manage : Save data Contract to file pdf
    */
    @RequestMapping(value = "/getpdf" ,method = RequestMethod.POST,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public void saveContractPDF(@RequestBody String json,HttpServletResponse response){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type","application/json; charset=utf-8");
        try{
            JSONObject jsonObject = new JSONObject(json);
            String today = jsonObject.getString("today");
            //change format date today
            int y = 543 + Integer.parseInt(today.subSequence(0, today.indexOf('/')).toString());
            String yyyy = Integer.toString(y);
            String mm = checkMM(today);

            String R_user = jsonObject.getString("R_user");
            String namePdf = jsonObject.getString("namePdf");
            int ageR = jsonObject.getInt("R_age");
            String R_age = Integer.toString(ageR);

            String R_number_home = jsonObject.getString("R_number_home");
            String R_alley = jsonObject.getString("R_alley");
            String R_road = jsonObject.getString("R_road");
            String R_district = jsonObject.getString("R_district");
            String R_prefecture = jsonObject.getString("R_prefecture");
            String R_county = jsonObject.getString("R_county");
            String O_user = jsonObject.getString("O_user");
            int ageO = jsonObject.getInt("O_age");
            String O_age = Integer.toString(ageO);

            String O_number_home = jsonObject.getString("O_number_home");
            String O_alley = jsonObject.getString("O_alley");
            String O_road = jsonObject.getString("O_road");
            String O_district = jsonObject.getString("O_district");
            String O_prefecture = jsonObject.getString("O_prefecture");
            String O_county = jsonObject.getString("O_county");
            String home_number = jsonObject.getString("home_number");
            String home_alley = jsonObject.getString("home_alley");
            String home_road = jsonObject.getString("home_road");
            String home_district = jsonObject.getString("home_district");
            String home_prefecture = jsonObject.getString("home_prefecture");
            String home_county = jsonObject.getString("home_county");
            String lengthDate = jsonObject.getString("lengthDate");
            String dateStart = jsonObject.getString("dateStart");
            // change format date dateStart
            int yStart = 543 + Integer.parseInt(today.subSequence(0, today.indexOf('/')).toString());
            String yyyyStart = Integer.toString(yStart);
            String mmStart = checkMM(dateStart);

            String dateEnd = jsonObject.getString("dateEnd");
            // change format date dateEnd
            int yEnd = 543 + Integer.parseInt(today.subSequence(0, today.indexOf('/')).toString());
            String yyyyEnd = Integer.toString(yEnd);
            String mmEnd = checkMM(dateEnd);

            String rent = jsonObject.getString("rent");
            String bail = jsonObject.getString("bail");

            int idUser = jsonObject.getInt("idUser");
            int idHome = jsonObject.getInt("idHome");

            String jrxmlFileName = "E:/Project_RoarRent/dev2/roarrent-webapp/src/main/webapp/reports/formatPDF/report1.jrxml";
            String jasperFileName = "E:/Project_RoarRent/dev2/roarrent-webapp/src/main/webapp/reports/formatPDF/report1.jasper";
            String pdfFileName = "E:/Project_RoarRent/dev2/roarrent-webapp/src/main/webapp/reports/contractPDF/"+namePdf+".pdf";

            JasperCompileManager.compileReportToFile(jrxmlFileName, jasperFileName);
            Map<String, Object> map = new HashMap<>();
            map.put("today_yyyy",yyyy);
            map.put("today_mm",mm);
            map.put("today",today.subSequence(today.lastIndexOf('/')+1, today.length()));

            map.put("nameUser_O",O_user);
            map.put("age_O",O_age);
            map.put("numberHome_O","บ้านเลขที่ "+O_number_home);
            map.put("alley_O",O_alley);
            map.put("road_O",O_road);
            map.put("district_O",O_district);
            map.put("prefecture_O",O_prefecture);
            map.put("county_O",O_county);

            map.put("nameUser_R",R_user);
            map.put("age_R",R_age);
            map.put("numberHome_R","บ้านเลขที่ "+R_number_home);
            map.put("alley_R",R_alley);
            map.put("road_R",R_road);
            map.put("district_R",R_district);
            map.put("prefecture_R",R_prefecture);
            map.put("county_R",R_county);

            map.put("numberHome",home_number);
            map.put("home_road",home_road);
            map.put("home_alley",home_alley);
            map.put("home_district",home_district);
            map.put("home_prefecture",home_prefecture);
            map.put("home_county",home_county);

            map.put("lengthDate",lengthDate);
            map.put("dateStart",dateStart.subSequence(dateStart.lastIndexOf('-')+1, dateStart.length()));
            map.put("yyyyStart",yyyyStart);
            map.put("mmStart",mmStart);

            map.put("dateEnd",dateEnd.subSequence(dateEnd.lastIndexOf('-')+1, dateEnd.length()));
            map.put("yyyyEnd",yyyyEnd);
            map.put("mmEnd",mmEnd);

            map.put("rent",rent);
            map.put("bail",bail);
            map.put("dateCost","5");
            String baht1 = new ThaiBaht().getText(rent);
            String baht2 = new ThaiBaht().getText(bail);
            map.put("thaiBaht1",baht1);
            map.put("thaiBaht2",baht2);

            JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasperFileName, map, new JREmptyDataSource());
            JasperExportManager.exportReportToPdfFile(jprint, pdfFileName);

            //  show pdf in web
            response.setContentType(MediaType.APPLICATION_PDF_VALUE);
            JasperExportManager.exportReportToPdfStream(jprint, response.getOutputStream());


        }catch (Exception e){
            e.printStackTrace();
        }
    }
    /*
    * method : saveInvoivePDF
    * Manage : Save data Invoive to file pdf
    */
    @RequestMapping(value = "/getInvoivePdf" ,method = RequestMethod.POST,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public void saveInvoivePDF(@RequestBody String json,HttpServletResponse response){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type","application/json; charset=utf-8");
        try{
            JSONObject jsonObject = new JSONObject(json);

            String R_user = jsonObject.getString("R_user");
            String name_ReceiptPdf = jsonObject.getString("name_ReceiptPdf");

            String home_number = jsonObject.getString("home_number");
            String home_alley = jsonObject.getString("home_alley");
            String home_road = jsonObject.getString("home_road");
            String home_district = jsonObject.getString("home_district");
            String home_prefecture = jsonObject.getString("home_prefecture");
            String home_county = jsonObject.getString("home_county");
            String home_name = jsonObject.getString("home_name");

            String dateStart = jsonObject.getString("dateStart");

            String bail = jsonObject.getString("bail");
            int n_bail = Integer.parseInt(bail);
            int cost_fire = jsonObject.getInt("cost_fire");
            int cost_water = jsonObject.getInt("cost_water");
            int cost_hire = jsonObject.getInt("cost_hire");
            int sum = n_bail+cost_hire;
            String sumCost = Integer.toString(sum);
            String baht1 = new ThaiBaht().getText(sumCost);

            String jrxmlFileName = "E:/Project_RoarRent/dev2/roarrent-webapp/src/main/webapp/reports/formatPDF/Receipt.jrxml";
            String jasperFileName = "E:/Project_RoarRent/dev2/roarrent-webapp/src/main/webapp/reports/formatPDF/Receipt.jasper";
            String pdfFileName = "E:/Project_RoarRent/dev2/roarrent-webapp/src/main/webapp/reports/invoivePDF/"+name_ReceiptPdf+".pdf";

            JasperCompileManager.compileReportToFile(jrxmlFileName, jasperFileName);
            Map<String, Object> map = new HashMap<>();

            map.put("name",R_user);

            map.put("address",home_number+" "+home_road+" "+home_alley+" "+home_district+" "+home_prefecture+" "+home_county);

            map.put("namefile",name_ReceiptPdf);
            map.put("date",dateStart);
            map.put("nameHome",home_name);

            map.put("fire","-");
            map.put("water","-");

            map.put("costFire",""+cost_fire);
            map.put("costWater",""+cost_water);
            map.put("costHire","");
            map.put("costBail","");

            map.put("fireAmount","");
            map.put("waterAmount","");
            map.put("hireAmount",""+cost_hire);
            map.put("bailAmount",bail);

            map.put("sum",sumCost);
            map.put("thaiSum",baht1);

            JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasperFileName, map, new JREmptyDataSource());
            JasperExportManager.exportReportToPdfFile(jprint, pdfFileName);

            //  show pdf in web
            response.setContentType(MediaType.APPLICATION_PDF_VALUE);
            JasperExportManager.exportReportToPdfStream(jprint, response.getOutputStream());


        }catch (Exception e){
            e.printStackTrace();
        }
    }
    /*
    * method : checkMM
    * Manage : change date number to text Thai Language
    */
    public static String checkMM(String day){
        String emblem = "";
        if (day.subSequence(4,5).equals("/")) {
            emblem = "/";
        }else if(day.subSequence(4,5).equals("-")){
            emblem = "-";
        }
        String mm =  "";
        if(day.subSequence(day.indexOf(emblem)+1,day.lastIndexOf(emblem) ).equals("01")){
            mm = "มกราคม";
        } else if(day.subSequence(day.indexOf(emblem)+1,day.lastIndexOf(emblem) ).equals("02")){
            mm = "กุมภาพันธ์";
        }else if(day.subSequence(day.indexOf(emblem)+1,day.lastIndexOf(emblem) ).equals("03")){
            mm = "มีนาคม";
        }else if(day.subSequence(day.indexOf(emblem)+1,day.lastIndexOf(emblem) ).equals("04")){
            mm = "เมษายน";
        }else if(day.subSequence(day.indexOf(emblem)+1,day.lastIndexOf(emblem) ).equals("05")){
            mm = "พฤษภาคม";
        }else if(day.subSequence(day.indexOf(emblem)+1,day.lastIndexOf(emblem) ).equals("06")){
            mm = "มิถุนายน";
        }else if(day.subSequence(day.indexOf(emblem)+1,day.lastIndexOf(emblem) ).equals("07")){
            mm = "กรกฎาคม";
        }else if(day.subSequence(day.indexOf(emblem)+1,day.lastIndexOf(emblem) ).equals("08")){
            mm = "สิงหาคม";
        }else if(day.subSequence(day.indexOf(emblem)+1,day.lastIndexOf(emblem) ).equals("09")){
            mm = "กันยายน";
        }else if(day.subSequence(day.indexOf(emblem)+1,day.lastIndexOf(emblem) ).equals("10")){
            mm = "ตุลาคม";
        }else if(day.subSequence(day.indexOf(emblem)+1,day.lastIndexOf(emblem) ).equals("11")){
            mm = "พฤศจิกายน";
        }else if(day.subSequence(day.indexOf(emblem)+1,day.lastIndexOf(emblem) ).equals("12")){
            mm = "ธันวาคม";
        }
        return mm;
    }
    /*
    * method : addHome
    * Manage : data send to restService
    */
    @RequestMapping(value = "/deleteContract",method = RequestMethod.POST,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public ResponseEntity<String> deleteContract(@RequestBody String json){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type","application/json; charset=utf-8");
        try{
            return restServiceOwn.deleteContract(json);
        }catch (Exception e){
            return  new ResponseEntity<String>("200",headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /*
    * method : getContract
    * Manage : get data Contract
    */
    @RequestMapping(value = "/getContract" , method = RequestMethod.GET,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public ResponseEntity<String> getContract(){
        ResponseEntity<String> responseEntity = homeService.getContract();
        return responseEntity;
    }

    /*
    * method : getDataUser
    * Manage : get data User
    */
    @RequestMapping(value = "/getDataUser" , method = RequestMethod.GET,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public ResponseEntity<String> getDataUser(){
        ResponseEntity<String> responseEntity = homeService.getDataUser();
        return responseEntity;
    }

    /*
    * method : editdatabank
    * Manage : update data bank
    */
    @RequestMapping(value = "/editdatabank" , method = RequestMethod.GET,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public ResponseEntity<String> editdatabank(){
        ResponseEntity<String> responseEntity = homeService.getDataBank();
        return responseEntity;
    }

    /*
    * method : editDataImage
    * Manage : update data bank
    */
    @RequestMapping(value = "/editDataImage" , method = RequestMethod.GET,produces = "text/html;charset=utf-8",headers = "Accept=application/json")
    public ResponseEntity<String> editDataImage(){
        ResponseEntity<String> responseEntity = homeService.getDataImage();
        return responseEntity;
    }

}
